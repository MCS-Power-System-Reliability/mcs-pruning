/*
 * main.cpp
 *
 *  Created on: Sep 23, 2009
 *      Author: Rob
 */

#include <iostream>
#include <iomanip>
#include <fstream>
#include <map>
#include <math.h>
#include <string>
#include <vector>
#include <memory>

// Utils
#include "anyoption.h"
#include "CommandLineUtils.h"
#include "LoggingUtils.h"
#include "MathUtils.h"
#include "PHEVUtils.h"
#include "SamplingUtils.h"
#include "StringUtils.h"
#include "SystemUtils.h"
#include "Utils.h"
#include "VectorUtils.h"

// State Space Samplers
#include "ATV_Sampler.h"
#include "PI_Sampler.h"
#include "CFO_Sampler.h"
#include "DS_Sampler.h"
#include "LDS_Sampler.h"
#include "LHS_Sampler.h"
#include "MCS_Sampler.h"
#include "MO_PSO_Sampler.h"
#include "PSO_Sampler.h"
#include "OMPB_Sampler.h"
#include "OMPP_Sampler.h"
#include "Sampler.h"

// State Classifiers
#include "OPFClassifier.h"
#include "SVMClassifier.h"
#include "CapacityClassifier.h"

// State Space Pruners
#include "ACOPruner.h"
#include "AISPruner.h"
#include "CFOPruner.h"
#include "GAPruner.h"
#include "ModifiedGAPruner.h"
#include "PSOPruner.h"
#include "RPSOPruner.h"
#include "MO_PSOPruner.h"
#include "MO_PSOPruner_Pareto.h"

using namespace std;
using namespace VectorUtils;
using namespace SamplingUtils;
using namespace StringUtils;
using namespace MathUtils;
using namespace SystemUtils;
using namespace LoggingUtils;
using namespace PHEVUtils;
using namespace Utils;
using namespace CommandLineUtils;

int main(int argc, char **argv){
    shared_ptr < Pruner >    p(nullptr);
    shared_ptr < MOPruner > mp(nullptr);
    vector < Generator > gens;
    vector < Line > lines;
    vector < Bus  > buses;
    vector < vector < int > > lineOutages, genOutages;
    map    < string, double > failedStates,successStates;
    char* aTime;
    char* aTime2;
    unique_ptr<AnyOption> opt(new AnyOption());
    CStopWatch timer;

    int pruningPopSize  = 25, pruningPopMax     = 25,  pruningPopStep = 5,
        pruningGenSize  = 25, pruningGenMax     = 25,  pruningGenStep = 5;
    int samplingPopSize = 25, samplingGenSize   = 1000, trials = 1;
    int run             = 0,    numBuses        = 0,
        numLines        = 0,    totalVehicles   = 1539000,
        numSamples      = 1000, numThreads      = 1,
        batchSize       = 0;
    int classThreads    = 1,    genThreads = 1; 

    double eLOLP, pruningProb = 0, pruningTime = 0, pLoad = 28.50, qLoad = 5.80, lineAdj = 1.0;
    double phevPenetration = 0.05, phevRho = 0.8;
    double totalTime, sigma = 0.025;
    double stoppingValue = 0;
    bool  useLines = false, useMoPruner = false,
          usePHEVs = false,
          saveGenOutages = false, saveLineOutages  = false,
          logConvergence = false, negateFitness    = false,
          useParetoMO    = false, useWeightedMO    = false,
          useLocalSearch = false, permuteSolutions = false;
    MTRand mt;

    shared_ptr < Sampler >    sampler(nullptr);
    shared_ptr < Classifier > classifier(nullptr);

    pruning::STOPPING_METHOD         stoppingMethod       = pruning::SM_GENERATION;
    pruning::PHEV_PLACEMENT          phevPlacement        = pruning::PP_EVEN_ALL_BUSES;
    pruning::PRUNING_METHOD          pruningMethod        = pruning::PM_NONE;
    pruning::SAMPLING_METHOD         samplingMethod       = pruning::SM_MCS;
    pruning::CLASSIFICATION_METHOD   classificationMethod = pruning::CM_OPF;
    pruning::PRUNING_OBJECTIVE       pruningObj           = pruning::PO_PROBABILITY;

    pruning::BUS_LOAD_DIST           loadDistType         = pruning::BL_PEAK;

    string fName       = "",
           logFileName = "",
           curSystem   = "RTS79",
           tString;
    ofstream myFile;

    /************************************ Begin Command Line Options ************************************/
    setUsage(opt); setOptions(opt);
    opt->processCommandArgs(argc, argv);

    if(opt->getFlag( "help" ) || opt->getFlag( 'h' ) ){
        opt->printUsage(); exit(0);
    }
    if(opt->getValue("useLines")        != NULL)    { useLines          = true;}
    if(opt->getValue("useMOPruner")     != NULL)    { useMoPruner       = true;}
    if(opt->getValue("useLocalSearch")  != NULL)    { useLocalSearch    = true;}
    if(opt->getValue("usePHEVs")        != NULL)    { usePHEVs          = true;}
    if(opt->getValue("saveGenOutages")  != NULL)    { saveGenOutages    = true;}
    if(opt->getValue("saveLineOutages") != NULL)    { saveLineOutages   = true;}
    if(opt->getValue("logConvergence")  != NULL)    { logConvergence    = true;}
    if(opt->getValue("negateFitness")   != NULL)    { negateFitness     = true;}
    if(opt->getValue("useParetoMO")     != NULL)    { useParetoMO       = true;}
    if(opt->getValue("useWeightedMO")   != NULL)    { useWeightedMO     = true;}
    if(opt->getValue("permute")         != NULL)    { permuteSolutions  = true;}
     
    // Parallel
    if(opt->getValue("numThreads")  != NULL){
        omp_set_dynamic(0);
        numThreads = atoi(opt->getValue("numThreads"));
        if(numThreads > omp_get_max_threads()-1){
            numThreads = omp_get_max_threads()-1; 
        }
    }else{
        numThreads = 1;
    }
    omp_set_num_threads(numThreads);

    if(opt->getValue("batchSize")  != NULL){ batchSize = atoi(opt->getValue("batchSize")); }
    else                                   { batchSize = 0; }
    // End Parallel

    // Parallel Pipeline
    if(opt->getValue("genThreads") != NULL) {
        genThreads = atoi(opt->getValue("genThreads"));
        if(genThreads < 1) { genThreads = 1; }
    }

    if(opt->getValue("classThreads") != NULL) {
        classThreads = atoi(opt->getValue("classThreads"));
        if(classThreads < 1) { classThreads = 1; }
    }
    
    if(opt->getValue("classThreads") != NULL || opt->getValue("genThreads") != NULL){
        numThreads = genThreads + classThreads + 1;
        omp_set_num_threads(numThreads);  
    }
    // End Parallel Pipeline

    if(useParetoMO && useWeightedMO){
        cout << "Invalid Combination of MO-Pruners (Can't use pareto and weighted together)!\n";
        exit(1);
    }

    if(usePHEVs){
        if(opt->getValue("penetration")  != NULL){
            phevPenetration = atof(opt->getValue("penetration"));
            if(phevPenetration < 0 || phevPenetration > 1){
                phevPenetration = 0.05;
            }
        }
        if(opt->getValue("rho")  != NULL){
            phevRho = atof(opt->getValue("rho"));
            if(phevRho < 0 || phevRho > 1){
                phevRho = 0.8;
            }
        }
        if(opt->getValue("totalVehicles")  != NULL){
            totalVehicles = atoi(opt->getValue("totalVehicles"));
            if(totalVehicles < 0){
                totalVehicles = 1539000;
            }
        }
        if(opt->getValue("placement")  != NULL){
            phevPlacement = Utils::getPHEVPlacement(opt->getValue("placement"));
        }else{
            phevPlacement = pruning::PP_EVEN_ALL_BUSES;
        }
    }else{
        phevPenetration = 0.00;
        phevRho         = 0.00;
        totalVehicles   = 0;
        phevPlacement   = pruning::PP_NONE;
    }
    if(opt->getValue("system")  != NULL){
        curSystem    = SamplingUtils::toUpper(opt->getValue("system"));
    }
    if(opt->getValue("lineAdj") != NULL){
        lineAdj = atof(opt->getValue("lineAdj"));
    }
    if(opt->getValue("classifier") != NULL){
        classificationMethod = Utils::getClassificationMethod(opt->getValue("classifier"));
    }

    if(opt->getValue("sampler")         != NULL){ samplingMethod  = Utils::getSamplingMethod(opt->getValue("sampler"));    }
    if(opt->getValue("samplingPop")     != NULL){ samplingPopSize = atoi(opt->getValue("samplingPop"));}
    if(opt->getValue("samplingGen")     != NULL){ samplingGenSize = atoi(opt->getValue("samplingGen"));}
    if(opt->getValue("sigma")           != NULL){ sigma           = atof(opt->getValue("sigma"));}
    if(opt->getValue("numSamples")      != NULL){ numSamples      = atoi(opt->getValue("numSamples"));}

    if(opt->getValue("loadDist")        != NULL) { loadDistType    = Utils::getBusLoadDist(opt->getValue("loadDist"));}

    if(opt->getValue("pruner") != NULL){
        pruningMethod = Utils::getPruningMethod(opt->getValue("pruner"));
    }
    if(opt->getValue("pruningObj") != NULL){
        pruningObj = Utils::getPruningObj(opt->getValue("pruningObj"));
    }
    if(opt->getValue("stoppingMethod") != NULL){
        stoppingMethod = Utils::getStoppingMethod(opt->getValue("stoppingMethod"));
    }
    if(opt->getValue("pruningPopMin")  != NULL){ pruningPopSize  = atoi(opt->getValue("pruningPopMin"));}
    if(opt->getValue("pruningPopMax")  != NULL){ pruningPopMax   = atoi(opt->getValue("pruningPopMax"));}
    if(opt->getValue("pruningPopStep") != NULL){ pruningPopStep  = atoi(opt->getValue("pruningPopStep"));}
    if(opt->getValue("pruningGenMin")  != NULL){ pruningGenSize  = atoi(opt->getValue("pruningGenMin"));}
    if(opt->getValue("pruningGenMax")  != NULL){ pruningGenMax   = atoi(opt->getValue("pruningGenMax"));}
    if(opt->getValue("pruningGenStep") != NULL){ pruningGenStep  = atoi(opt->getValue("pruningGenStep"));}

    if(opt->getValue("stoppingValue")  != NULL){ stoppingValue = atof(opt->getValue("stoppingValue"));}
    if(opt->getValue("trials") != NULL)        { trials        = atoi(opt->getValue("trials"));}

    /************************************ End Command Line Options ************************************/

    SystemUtils::loadSystemData(pLoad, qLoad, numBuses, numLines, curSystem, gens, lines, buses);

    if(classificationMethod == pruning::CM_OPF)     { classifier = make_shared<OPFClassifier>(curSystem, numBuses, gens, buses, lines);}
    else if(classificationMethod == pruning::CM_SVM){ classifier = make_shared<SVMClassifier>(curSystem, numBuses, gens, buses, lines);}
    else if(classificationMethod == pruning::CM_CAP){ classifier = make_shared<CapacityClassifier>(curSystem, numBuses, gens, buses, lines, pLoad);}
    classifier->setUseLines(useLines);

    /************************************ Begin File Setup ************************************/
    aTime = new char[20];
    LoggingUtils::getTimeStamp(aTime);
    fName = LoggingUtils::getFileName("../Results/", curSystem, Utils::getSamplingMethodString(samplingMethod), 
                                        Utils::getPruningMethodString(pruningMethod), Utils::getClassificationMethodString(classificationMethod),
                                        Utils::getPruningObjString(pruningObj), useLines, useMoPruner, aTime, useLocalSearch, usePHEVs, 
                                        negateFitness, phevPenetration, phevPlacement, numThreads);
                        
    myFile.open(fName.c_str(), ios::trunc);
    myFile << "RunNum Population Generations SuccessStates FailureStates StatesSlope ProbSlope SamplerLOLP ";
    myFile << "PrunedLOLP EstimateLOLP PruningTime StateGenerationTime SearchTime ClassificationTime ";
    myFile << "SimulationTime TotalTime SamplerIterations PruningIterations ";
    myFile << "Collisions AvgNumLinesOut LineAdj AvgNumGensOut PHEVPenetration PHEVRho numThreads ";
    myFile << "System Sampler Classifier Pruner PruningObjective LineFailures MultiObj LocalSearch NegateFitness PhevPlacement BatchSize ";
    myFile << "genThreads classThreads loadDist";
    if(classificationMethod == pruning::CM_SVM){
        myFile << "Sensitvity Specificity Accuracy Precision ";
    }
      
    myFile << endl;
    /************************************ End File Setup ************************************/

    cout    << fixed
            << setw(10) << left << "Run "
            << setw(20) << left << "Sampler ";
            
    if(pruningMethod != pruning::PM_NONE){
        cout    << fixed
                << setw(10) << left << "Pruning"
                << setw(5)  << left << "Pop "
                << setw(5)  << left << "Gen "
                << setw(10) << left << "SS"
                << setw(10) << left << "FS  ";
    }

    cout    << setw(10) << left << "System "
            << setw(10) << left << "LOLP"
            << setw(10) << left << "Sigma"
            << setw(10) << left << "Samples";

    if(samplingMethod != pruning::SM_MCS_OMPB){
        cout    << setw(15) << left << "Unique States"
                << setw(15) << left << "Collisions";
    }

    if(samplingMethod == pruning::SM_MCS_OMPP){
        cout   << setw(15) << "G. Threads";
        cout   << setw(15) << "C. Threads";
    }
    if(samplingMethod == pruning::SM_MCS_OMPB){
        cout   << setw(15) << left << "Threads";           
    }

    if(samplingMethod == pruning::SM_MCS_OMPB || samplingMethod == pruning::SM_LHS){
        cout   << setw(15) << left << "Batch Size";
    }

    cout    << setw(15) << left << "Load Dist."
            << setw(15) << left << "Sampler Time"
            << setw(15) << left << "Total Time"
            << endl;

    for(; pruningPopSize <= pruningPopMax; pruningPopSize += pruningPopStep){
        for(int generations = pruningGenSize; generations <= pruningGenMax; generations += pruningGenStep){
            for(int t=0; t<trials; t++){
                run++;

                timer.startTimer();

                // Reset!
                failedStates.clear(); successStates.clear();
                classifier->init();
                classifier->resetTimes();
                pruningProb    = 0;
                if(stoppingMethod == pruning::SM_GENERATION){ stoppingValue = generations;} // Just in case

                if(useMoPruner){
                    if(pruningMethod == pruning::PM_GA){
                        // TODO
                    }else if(pruningMethod == pruning::PM_MGA){
                        // TODO
                    }else if(pruningMethod == pruning::PM_PSO){
                        if(mp == NULL){
                            if(!useWeightedMO && !useParetoMO){
                                p = std::make_shared<MO_PSOPruner>(pruningPopSize, generations, classifier, gens, lines, pLoad, useLines);
                            }else if(useParetoMO){
                                p = std::make_shared<MO_PSOPruner_Pareto>(pruningPopSize, generations, classifier, gens, lines, pLoad, useLines);
                            }else if (useWeightedMO){
                                //TODO
                                cout << "Combination does not yet exist!\n";
                                exit(1);
                            }
                        }else{
                            p->Reset(pruningPopSize, generations);
                        }
                    }else if(pruningMethod == pruning::PM_RPSO){
                        // TODO
                        cout << "Pruning method does not yet exist!\n";
                        exit(1);
                    }else if(pruningMethod == pruning::PM_ACO){
                        //TODO
                        cout << "Pruning method does not yet exist!\n";
                        exit(1);
                    }
                }else{
                    if(pruningMethod == pruning::PM_GA){
                        if(p == NULL) { p = std::make_shared<GAPruner>(pruningPopSize, generations, classifier, gens, lines, pLoad, useLines); }
                        else          { p->Reset(pruningPopSize, generations); }
                    
                    }else if(pruningMethod == pruning::PM_MGA){
                        if(p == NULL) { p = std::make_shared<ModifiedGAPruner>(pruningPopSize, generations, classifier, gens, lines, pLoad, useLines); }
                            else      { p->Reset(pruningPopSize, generations); }
                    
                    }else if(pruningMethod == pruning::PM_PSO){
                        if(p == NULL){
                            p = std::make_shared<PSOPruner>(pruningPopSize, generations, classifier, gens, lines, pLoad, useLines);
                            //((PSOPruner*)p)->C1 = 4;((PSOPruner*)p)->C2 = 10; ((PSOPruner*)p)->W = 3;
                        }else{
                            p->Reset(pruningPopSize, generations);
                        }
                    
                    }else if(pruningMethod == pruning::PM_RPSO){
                        if(p == NULL) { p = std::make_shared<RPSOPruner>(pruningPopSize, generations, classifier, gens, lines, pLoad, useLines); }
                        else          { p->Reset(pruningPopSize, generations); }
                    
                    }else if(pruningMethod == pruning::PM_ACO){
                        if(p == NULL) { p = std::make_shared<ACOPruner>(pruningPopSize, generations, classifier, gens, lines, pLoad,  stoppingMethod, useLines); }
                        else          { p->Reset(pruningPopSize, generations); }

                    }else if(pruningMethod == pruning::PM_AIS){
                        if(p == NULL) { p = std::make_shared<AISPruner>(pruningPopSize, generations, classifier, gens, lines, pLoad, useLines, .35, .35); }
                        else          { p->Reset(pruningPopSize, generations); }

                    }else if(pruningMethod == pruning::PM_CFO){
                        if(p == NULL) { p = std::make_shared<CFOPruner>(pruningPopSize, generations, classifier, gens, lines, pLoad, useLines); }
                        else          { p->Reset(pruningPopSize, generations); }
                    }
                }

                if(p != NULL){
                    p->setUseLogging(logConvergence);
                    p->setNegateFitness(negateFitness);
                    p->setLineAdjustment(lineAdj);
                    p->setuseLocalSearch(useLocalSearch);
                    p->setObjective(pruningObj);
                    p->setStoppingMethod(stoppingMethod);
                    p->setStoppingValue(stoppingValue);
                    p->setUsePHEVs(usePHEVs);
                    p->setPHEVPlacement(phevPlacement);
                    p->setNumBuses(numBuses);
                    p->setPermuteSolutions(permuteSolutions);
                    p->setNumThreads(numThreads);
                    p->setLoadDistType(loadDistType); 
                    p->Prune(mt);
                    pruningTime   = p->getPruningTime();
                    successStates = p->getSuccessStates();
                    failedStates  = p->getFailureStates();
                }

                if(sampler == NULL){
                    if(samplingMethod == pruning::SM_ACO)      { cout << "Sampler does not yet exist!\n"; exit(1);}
                    else if(samplingMethod == pruning::SM_AIS) { cout << "Sampler does not yet exist!\n"; exit(1);}
                    else if(samplingMethod == pruning::SM_GA)  { cout << "Sampler does not yet exist!\n"; exit(1);}
                    else if(samplingMethod == pruning::SM_MGA) { cout << "Sampler does not yet exist!\n"; exit(1);}
                    else if(samplingMethod == pruning::SM_MCS) { sampler = make_shared<MCS_Sampler>();}
                    else if(samplingMethod == pruning::SM_LHS) { 
                        sampler = std::make_shared<LHS_Sampler>();
                        std::dynamic_pointer_cast<LHS_Sampler>(sampler)->setNumSamples(numSamples);    
                        std::dynamic_pointer_cast<LHS_Sampler>(sampler)->setBatchSize(batchSize);  

                    }else if(samplingMethod == pruning::SM_DS){
                        sampler = std::make_shared<DS_Sampler>();
                        std::dynamic_pointer_cast<DS_Sampler>(sampler)->setNumSamples(numSamples);  

                    }else if(samplingMethod == pruning::SM_HAL || samplingMethod == pruning::SM_HAM || 
                            samplingMethod == pruning::SM_FAU || samplingMethod == pruning::SM_SOBOL){
                        
                        sampler = std::make_shared<LDS_Sampler>(samplingMethod, numSamples);

                    }else if(samplingMethod == pruning::SM_RPSO) { cout << "Sampler does not yet exists!\n"; exit(1);
                    }else if(samplingMethod == pruning::SM_MO_PSO){ 
                        if(useLines && lines.size() > 0){ sampler = std::make_shared<MO_PSO_Sampler>(samplingPopSize, gens.size()+lines.size(), samplingGenSize); }
                        else                            { sampler = std::make_shared<MO_PSO_Sampler>(samplingPopSize, gens.size(), samplingGenSize);}
                    }
                    else if(samplingMethod == pruning::SM_PSO){ 
                        if(useLines && lines.size() > 0){ 
                            sampler = std::make_shared<PSO_Sampler>(samplingPopSize, gens.size()+lines.size(), samplingGenSize); 
                        }
                        else{ 
                            sampler = std::make_shared<PSO_Sampler>(samplingPopSize, gens.size(), samplingGenSize);
                        }

                    }
                    else if(samplingMethod == pruning::SM_CFO){ 
                        if(useLines && lines.size() > 0){ sampler = std::make_shared<CFO_Sampler>(samplingPopSize, gens.size()+lines.size(), samplingGenSize); }
                        else                            { sampler = std::make_shared<CFO_Sampler>(samplingPopSize, gens.size(), samplingGenSize);}    
                    }else if(samplingMethod == pruning::SM_PI)      { sampler = std::make_shared<PI_Sampler>(); 
                    }else if(samplingMethod == pruning::SM_ATV)     { sampler = std::make_shared<ATV_Sampler>(); 
                    }else if(samplingMethod == pruning::SM_MCS_OMPB){ 
                        sampler = std::make_shared<OMPB_Sampler>(); 
                        ((OMPB_Sampler*)sampler.get())->setBatchSize(batchSize);

                    }else if(samplingMethod == pruning::SM_MCS_OMPP){ 
                        sampler = std::make_shared<OMPP_Sampler>(genThreads, classThreads); 
                    }
                }
                sampler->setCurrentSystem(curSystem);
                sampler->Init(failedStates, successStates, gens, lines, pLoad, classifier, numBuses, totalVehicles, phevPenetration, phevRho);
                sampler->setUseLines(useLines);
                sampler->setUsePHEVs(usePHEVs);
                sampler->setLineAdjustment(lineAdj);
                sampler->setPHEVPlacement(phevPlacement);
                sampler->setTolerance(sigma);
                sampler->setUseLogging(logConvergence); 
                sampler->setLoadDistType(loadDistType);               
                sampler->run(mt);

                timer.stopTimer();
                totalTime = timer.getElapsedTime();

                if(useLines){lineOutages.push_back(sampler->getTLineOutageCounts());}
                genOutages.push_back(sampler->getGenOutageCounts());

                //Calculate LOLP
                if(p != NULL){pruningProb = p->getPrunedProb();}
                else         {pruningProb = 0;}
                eLOLP = sampler->getLOLP() * (1 - pruningProb);

                /************************************ Begin File Output ************************************/
                myFile << run << " ";
                if(p != NULL){
                    myFile << pruningPopSize        << " "
                           << generations           << " "
                           << successStates.size()  << " "
                           << failedStates.size()   << " ";
                }else{
                    myFile << 0 << " "
                           << 0 << " "
                           << 0 << " "
                           << 0 << " ";
                }

                if(p != NULL){
                    myFile  << setprecision(7)
                            << p->getStatesSlope()  << " "
                            << p->getProbSlope()    << " ";
                }else{
                    myFile  << setprecision(7)
                            << 0 << " "
                            << 0 << " ";
                }

                myFile << setprecision(7) << sampler->getLOLP() << " ";
                if(p != NULL){
                    myFile  << setprecision(7)
                            << (1 - pruningProb)    << " "
                            << eLOLP                << " "
                            << p->getPruningTime()  << " ";
                }else{
                    myFile  << setprecision(7)
                            << 0 << " "
                            << 0 << " "
                            << 0 << " ";
                }

                myFile    << setprecision(7)
                        << sampler->getGenTime()            << " "
                        << sampler->getSearchTime()         << " "
                        << sampler->getClassificationTime() << " " 
                        << sampler->getSimTime()            << " "
                        << totalTime                        << " ";
                myFile  << setprecision(0)
                        << sampler->getTotalIterations()    << " ";

                if(p != NULL){
                    myFile  << p->getTotalIterations()      << " "
                            << sampler->getTotalCollisions() << " ";
                }else{
                    myFile << 0 << " "
                            << 0 << " ";
                }
                
                if(useLines){
                    myFile << setprecision(3)
                           << sampler->getAvgLinesOut()     << " "
                           << sampler->getLineAdjustment()  << " ";
                }else{
                    myFile << setprecision(3)
                           << 0 << " "
                           << 0 << " ";
                }
                myFile << sampler->getAvgGensOut() << " ";
                
                if(usePHEVs){
                    myFile << fixed << setprecision(3)
                           << phevPenetration   << " "
                           << phevRho           << " ";
                }else{
                    myFile << fixed << setprecision(2)
                           << 0    << " "
                           << 0    << " ";
                }
                myFile << numThreads << " ";
                myFile << curSystem  << " "
                       << Utils::getSamplingMethodString(samplingMethod)            << " "
                       << Utils::getClassificationMethodString(classificationMethod)<< " "
                       << Utils::getPruningMethodString(pruningMethod)              << " "
                       << Utils::getPruningObjString(pruningObj)                    << " "
                       << Utils::getBooleanString(useLines)                         << " "
                       << Utils::getBooleanString(useMoPruner)                      << " "
                       << Utils::getBooleanString(useLocalSearch)                   << " "
                       << Utils::getBooleanString(negateFitness)                    << " "
                       << Utils::getPHEVPlacementString(phevPlacement)              << " ";

                if(classificationMethod == pruning::CM_SVM){
                    myFile << fixed << setprecision (4)
                           << ((SVMClassifier*)classifier.get())->getSensitivity()    << " "
                           << ((SVMClassifier*)classifier.get())->getSpecificity()    << " "
                           << ((SVMClassifier*)classifier.get())->getAccuracy()       << " "
                           << ((SVMClassifier*)classifier.get())->getPrecision()      << " ";
                }

                myFile << batchSize << " ";
                
                if(samplingMethod == pruning::SM_MCS_OMPP){
                    myFile << ((OMPP_Sampler*)sampler.get())->getGenThreads()         << " ";
                    myFile << ((OMPP_Sampler*)sampler.get())->getClassThreads()       << " ";
                }else{
                    myFile << 0 << " ";
                    myFile << 0 << " ";
                }

                myFile << Utils::getBusLoadDistString(loadDistType);
                
                myFile << endl;

                if(logConvergence){
                    aTime2 = new char[20];
                    LoggingUtils::getTimeStamp(aTime2);
                    if(p != NULL){
                        logFileName = LoggingUtils::getFileName("../Results", curSystem, Utils::getSamplingMethodString(samplingMethod), Utils::getPruningMethodString(pruningMethod),
                                Utils::getClassificationMethodString(classificationMethod), Utils::getPruningObjString(pruningObj),
                                useLines, useMoPruner, aTime2, useLocalSearch, usePHEVs, negateFitness, phevPenetration, phevPlacement, numThreads);
                        logFileName = logFileName.replace(logFileName.end()- 4,logFileName.end(), "_Pruner_Convergence.csv");
                        p->writeLog(logFileName);
                    }
                    logFileName = LoggingUtils::getFileName("../Results", curSystem, Utils::getSamplingMethodString(samplingMethod), 
                                                        Utils::getPruningMethodString(pruningMethod),
                                                        Utils::getClassificationMethodString(classificationMethod), 
                                                        Utils::getPruningObjString(pruningObj),
                                                        useLines, useMoPruner, aTime2, useLocalSearch, usePHEVs, negateFitness, 
                                                        phevPenetration, phevPlacement, numThreads);
                    logFileName = logFileName.replace(logFileName.end()- 4,logFileName.end(), "_Sampler_Convergence.csv");
                    sampler->writeLog(logFileName);

                    delete aTime2;
                }
                /************************************ End File Output ************************************/

                cout    << fixed
                        << setprecision(0) << setw(10) << left << run
                        << setprecision(0) << setw(20) << left << Utils::getSamplingMethodString(samplingMethod);
                        
                if(p != NULL){
                    cout    << fixed
                            << setprecision(0) << setw(10) << left << Utils::getPruningMethodString(pruningMethod)
                            << setw(5)  << left << pruningPopSize
                            << setw(5)  << left << generations
                            << setw(10) << successStates.size() 
                            << setw(10) << failedStates.size();
                }

                cout    << setprecision(0) << setw(10) << left << curSystem
                        << setprecision(4) << setw(10) << left << eLOLP
                        << setprecision(4) << setw(10) << left << sampler->getSigma() 
                        << setprecision(0) << setw(10) << left << sampler->getTotalIterations();
                
                if(samplingMethod != pruning::SM_MCS_OMPB){
                    cout    << setprecision(0) << setw(15) << left << sampler->getNumStatesSampled()
                            << setprecision(0) << setw(15) << left << sampler->getTotalCollisions();
                }

                if(samplingMethod == pruning::SM_MCS_OMPP){
                    cout << setprecision(0) << setw(15) << left << genThreads;
                    cout << setprecision(0) << setw(15) << left << classThreads;
                }
                if(samplingMethod == pruning::SM_MCS_OMPB){
                    cout << setprecision(0) << setw(15) << left << numThreads;
                }
                if(samplingMethod == pruning::SM_MCS_OMPB || samplingMethod == pruning::SM_LHS){
                    cout << setprecision(0) << setw(15) << left << batchSize;
                }
                cout    << setw(15) << left << Utils::getBusLoadDistString(loadDistType)
                        << setprecision(2) << setw(15) << left << sampler->getSimTime()
                        << setprecision(2) << setw(15) << left << totalTime << endl;

                if(pruningMethod == pruning::PM_PSO && useParetoMO){
                    ((MO_PSOPruner_Pareto*)p.get())->printArchive();
                }
            }
        }
    }
    myFile.close();
    if(saveLineOutages && useLines){
        aTime2 = new char[20];
        LoggingUtils::getTimeStamp(aTime2);
        LoggingUtils::writeLineOutages("../Results", curSystem, Utils::getSamplingMethodString(samplingMethod),
                Utils::getPruningMethodString(pruningMethod), Utils::getClassificationMethodString(classificationMethod),
                Utils::getPruningObjString(pruningObj),
                useLines, useMoPruner, aTime2, useLocalSearch, usePHEVs, negateFitness, phevPenetration, phevPlacement, lineOutages, lines, numThreads);
        delete aTime2;
    }
    if(saveGenOutages){
        aTime2 = new char[20];
        LoggingUtils::writeGeneratorOutages("../Results", curSystem, Utils::getSamplingMethodString(samplingMethod),
                Utils::getPruningMethodString(pruningMethod), Utils::getClassificationMethodString(classificationMethod),
                Utils::getPruningObjString(pruningObj),
                useLines, useMoPruner, aTime2, useLocalSearch, usePHEVs, negateFitness, phevPenetration, phevPlacement, genOutages, gens, numThreads);
        delete aTime2;
    }
    
    // Clean Up
    // delete classifier;
    // delete p;
    delete aTime;
    // delete opt;

    return 0;
}

