/*
 * MCS.cpp
 *
 *  Created on: Feb 5, 2010
 *      Author: rgreen
 */

#include "OMPB_Sampler.h"
#include <iostream>
#include <iomanip>

using namespace std;

OMPB_Sampler::OMPB_Sampler() : Sampler() {
    _batchSize = 100;
}

OMPB_Sampler::~OMPB_Sampler() {

}

void OMPB_Sampler::run(MTRand& mt){
    double     sumX,  sumXSquared, nSumX, nSumXSquared,
            curtailment, excess, totalCap, sigma, nSigma, sigmaEENS, stateProb;
    bool     failed;
    int     innerLoopMax;
    std::string curSolution;
    std::vector<double> vCurSolution;
    std::vector<double> phevLoad, phevGen;

    std::vector<Generator>     localGens;
    std::vector<Line>         localLines;

    curSolution = "";
    failed = false;
    vCurSolution.resize(gens.size() + lines.size() + 2, 0);
    sumX=0; sumXSquared=0; nSumX=0; nSumXSquared=0;
    curProb      = 0.0; 
    numSamples      = 1;     iterations      = 0;    sigma = 1.0;
    nSigma       = 1.0;    
    Collisions     = 0;    avgLineCount = 0;    avgGenCount  = 0;
    stateGenerationTime = searchTime = 0;

    vLOLP = 0; pLOLP  = 0; LOLP   = 0;
    NLOLP = 0; vNLOLP = 0; pNLOLP = 0;
    EENS  = 0; pEENS  = 0; vEENS  = 0;
    
    lolps.clear(); sigmas.clear(); vLolps.clear();
    nLolps.clear(); nSigmas.clear();

    srand((unsigned)time(NULL));

    localGens  = gens;
    localLines = lines;

    CStopWatch timer1;
    classifier->reset();
    
    double localNumSamples = 1;   
    double localIterations = 0;
    double localStateGenerationTime = 0;
    double localClassificationTime = 0;

    timer.startTimer();
    while(true){

        /******************************************** Generate States********************************************/
       
        #pragma omp parallel private(timer1, curSolution, curtailment, stateProb) \
                             shared(localLines, localGens) \
                             reduction(+:localStateGenerationTime, localClassificationTime, localNumSamples, localIterations, sumX, sumXSquared, nSumX, nSumXSquared)
        {
            int threadId = omp_get_thread_num();
            Classifier* localClassifier = classifier->clone();

            MTRand mt;
            mt.seed(mt.randInt(threadId + numSamples));

            for(int innerLoops=0; innerLoops<_batchSize; innerLoops++){
                
                curSolution = "";
                totalCap    = 0.00;
                
                timer1.startTimer();
                stateProb = 1; genCount = 0;
                for(int x=0; x< localGens.size(); x++){           
                    curProb = mt.rand();
                    if(curProb <=  localGens[x].getOutageRate()){ curSolution += '0';}
                    else                                        { curSolution += '1';}
                }

                if(useLines && localLines.size() > 0){
                    for(int x=0; x<localLines.size(); x++){
                        curProb = mt.rand();
                        if(curProb <= (localLines[x].getOutageRate()*lineAdjustment)){
                            curSolution += '0';
                        }else{
                            curSolution += '1';
                        }
                    }
                }
                timer1.stopTimer();
                stateGenerationTime += timer1.getElapsedTime();

                /******************************************** Classify States********************************************/
       
                timer1.startTimer();
                // cout << "Before Class\n";
                adjustBusLoads(mt);
                curtailment = localClassifier->run(curSolution, excess);
                // cout << curtailment << " " << curSolution << endl;
                timer1.stopTimer();
                classificationTime += timer1.getElapsedTime();

                if(curtailment > 0) { sumX++;  sumXSquared++; }
                else                { nSumX++; nSumXSquared++;}

                localNumSamples++; localIterations++; 

            }

            delete localClassifier;
        }

        numSamples = localNumSamples;
        iterations = localIterations;

        pLOLP = LOLP;
        LOLP  = sumX/numSamples;
        vLOLP = (1.0/numSamples) * (sumXSquared/numSamples - pow(LOLP,2.0));
        sigma = sqrt(vLOLP)/LOLP;

        pNLOLP = NLOLP;
        NLOLP  = nSumX/numSamples;
        vNLOLP = (1.0/numSamples) * (nSumXSquared/numSamples - pow(NLOLP,2.0));
        nSigma = sqrt(vNLOLP)/NLOLP;
        
        vLolps.push_back(vLOLP);
        lolps .push_back(LOLP);
        sigmas.push_back(sigma);

        nLolps .push_back(NLOLP);
        nSigmas.push_back(nSigma);

        if(sigma < tolerance) { break;}
    }  

    stateGenerationTime = localStateGenerationTime;
    classificationTime = localClassificationTime;

    timer.stopTimer();
    simulationTime = timer.getElapsedTime();
}


void OMPB_Sampler::setBatchSize(int batchSize){
    _batchSize = batchSize;
}