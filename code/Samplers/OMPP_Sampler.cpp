/*
 * MCS.cpp
 *
 *  Created on: Feb 5, 2010
 *      Author: rgreen
 */

#include "OMPP_Sampler.h"
#include <iostream>
#include <iomanip>
#include <queue>

//#include <boost/thread.hpp>
//#include <boost/lockfree/queue.hpp>

using namespace std;

OMPP_Sampler::OMPP_Sampler() : Sampler() {


}

OMPP_Sampler::OMPP_Sampler(int gThreads, int cThreads) : Sampler() {

  if(gThreads > 0){ genThreads   = gThreads;}
  if(cThreads > 0){ classThreads = cThreads;}

}

OMPP_Sampler::~OMPP_Sampler() {

}

void OMPP_Sampler::run(MTRand& mt){
    double  sumX,  sumXSquared, nSumX, nSumXSquared,
            curtailment, excess, totalCap, sigma, nSigma, sigmaEENS, stateProb;
    bool    failed;
    std::string curSolution;
    std::vector<double> vCurSolution;
    std::vector<double> phevLoad, phevGen;
    
    std::queue < std::vector <double> > q1;
    //queue<string> q1;
    queue<double> cq; 

    curSolution = "";
    failed = false;
    vCurSolution.resize(gens.size() + lines.size() + 2, 0);
    sumX=0; sumXSquared=0; nSumX=0; nSumXSquared=0;
    curProb      = 0.0; 
    numSamples   = 1;   iterations   = 0;   sigma = 1.0;
    nSigma       = 1.0; 
    Collisions   = 0;   avgLineCount = 0;   avgGenCount  = 0;
    stateGenerationTime = searchTime = 0;

    vLOLP = 0; pLOLP  = 0; LOLP   = 1;
    NLOLP = 0; vNLOLP = 0; pNLOLP = 0;
    EENS  = 0; pEENS  = 0; vEENS  = 0;

    int flag=0;
    tLineOutageCounts.clear();       genOutageCounts.clear();
    uniqueStates.clear();            sampledStates.clear();
    uniqueFailedStatesCount.clear(); uniqueSuccessStatesCount.clear();
    fStatesCount.clear();            sStatesCount.clear();
    localFailedStates.clear();       localSuccessStates.clear();
    sampledStateOccurrences.clear(); stateCurtailment.clear();

    tLineOutageCounts.resize(lines.size()+1, 0); genOutageCounts.resize(gens.size()+1, 0);

    lolps.clear(); sigmas.clear(); vLolps.clear();
    nLolps.clear(); nSigmas.clear();

    double EDNS = 0, LOEE = 0,
           LOLE = 0, DNS  = 0,
           iK   = 0, PLC  = 0;
    timer.startTimer();
    omp_set_num_threads(genThreads+classThreads+1);
     
    srand(time(NULL));

    double lStateGenerationTime = 0.0, lClassificationTime = 0.0;

    timer.startTimer();
    #pragma omp parallel private(curSolution, vCurSolution, curtailment, excess) reduction(+ : lStateGenerationTime) reduction(+ : lClassificationTime)
    {
      
        int threadId = omp_get_thread_num();
    /******************************************** Generate States********************************************/
        if(threadId >= 0 && threadId < genThreads){     
            
            //Use local copy without auto-init for thread safety
            MTRand myMt(rand()+threadId);
            CStopWatch genTimer;
            
            if(useLines){ vCurSolution.resize(gens.size()+lines.size(), 0); }
            else        { vCurSolution.resize(gens.size(), 0); }
            genTimer.startTimer();
            while(!flag){

                unsigned int x = 0;
                stateProb      = 1; 
                genCount       = 0;

                for(x=0; x< gens.size(); x++){
                    curProb = myMt.rand();
                    if(curProb <= gens[x].getOutageRate()){ vCurSolution[x] = 0;}
                    else                                  { vCurSolution[x] = 1;} 
                }
              
                if(useLines && lines.size() > 0){
                    for(unsigned int x=0; x<lines.size(); x++){ 
                        curProb = myMt.rand();
                        if(curProb <= (lines[x].getOutageRate()*lineAdjustment)){ vCurSolution[gens.size()+x] = 0; }
                        else                                                    { vCurSolution[gens.size()+x] = 1; }   
                    }  
                }
                #pragma omp critical (firstQueue)
                { 
                    sampledStateProbs[curSolution] = stateProb;
                    q1.push(vCurSolution);
                }
            }
            genTimer.stopTimer();
            lStateGenerationTime += genTimer.getElapsedTime();
        }else if(threadId >= genThreads && threadId <(genThreads+classThreads)){
/******************************************** Classify  States********************************************/
            Classifier* localC = classifier->clone();
            CStopWatch  cTimer;
            //string newCurSolution;
            cTimer.startTimer();
            while(!flag){
                if(!q1.empty()){
                    //string newCurSolution;
                 
                    #pragma omp critical (firstQueue)
                    {
                        //curSolution = q1.front(); 
                        vCurSolution = q1.front();
                        q1.pop();
                    }     
                    adjustBusLoads(mt);
                    curtailment = localC->run(vCurSolution, excess);

                    #pragma omp critical (secondQueue)
                    {  
                        cq.push(curtailment);   
                    }     
                }
            }
            cTimer.stopTimer();
            lClassificationTime += cTimer.getElapsedTime();
        }else if(threadId == (genThreads+classThreads)){
           
            /******************************************** Computation********************************************/
            CStopWatch compTimer;
            compTimer.startTimer();
            while(!flag){
                if(!cq.empty()){
           
                    #pragma omp critical (secondQueue)
                    {   
                        curtailment = cq.front();
                        cq.pop();
                    } 

                    if(curtailment > 0) { sumX++;  sumXSquared++; }
                    else                { nSumX++; nSumXSquared++; }

                    numSamples++; iterations++;
                    
                    pLOLP = LOLP;
                    LOLP  = sumX/numSamples;
                    vLOLP = (1.0/numSamples) * (sumXSquared/numSamples - pow(LOLP,2));
                    sigma = sqrt(vLOLP)/LOLP;

                    pNLOLP = NLOLP;
                    NLOLP  = nSumX/numSamples;
                    vNLOLP = (1/numSamples) * (nSumXSquared/numSamples - pow(NLOLP,2));
                    nSigma = sqrt(vNLOLP)/NLOLP;


                    vLolps.push_back(vLOLP);
                    lolps .push_back(LOLP);
                    sigmas.push_back(sigma);

                    nLolps .push_back(NLOLP);
                    nSigmas.push_back(nSigma);

                    if(sigma < tolerance) {
                        #pragma omp critical (flagCS) 
                        {
                            flag=1;
                        }
                    }
/*--------------------------------------------------------------------------------------------------------------------------------------------------------------*/  
                }
            }
            vCurSolution.clear();
        }
    }

    timer.stopTimer();
    simulationTime = timer.getElapsedTime();

    stateGenerationTime = lStateGenerationTime;
    classificationTime  = lClassificationTime;
}

int OMPP_Sampler::getClassThreads() {return classThreads;}
int OMPP_Sampler::getGenThreads()   {return genThreads;}
