/** OpenMP Batch Sampler **/

#ifndef OMPB_SAMPLER_H_
#define OMPB_SAMPLER_H_

#include <math.h>
#include <map>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <vector>

#include "Classifier.h"
#include "defs.h"
#include "Generator.h"
#include "CStopWatch.h"
#include "Line.h"
#include "MTRand.h"
#include "OPFClassifier.h"
#include "Sampler.h"

class OMPB_Sampler : public Sampler{
    public:
        OMPB_Sampler();
        virtual ~OMPB_Sampler();
        virtual void run(MTRand& mt);

        void setBatchSize(int batchSize);

    private:
        int _batchSize;
};

#endif /* OMPB_Sampler_H_ */
