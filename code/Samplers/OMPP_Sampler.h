/*
 * MCS_Sampler.h
 *
 *  Created on: Feb 5, 2010
 *      Author: rgreen
 */

#ifndef OMPP_SAMPLER_H_
#define OMPP_SAMPLER_H_

#include <math.h>
#include <map>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <vector>
#include <omp.h>

#include "Classifier.h"
#include "defs.h"
#include "Generator.h"
#include "CStopWatch.h"
#include "Line.h"
#include "MTRand.h"
#include "OPFClassifier.h"
#include "Sampler.h"

class OMPP_Sampler : public Sampler{
    public:
        OMPP_Sampler();
        OMPP_Sampler(int gThreads, int cThreads);
        virtual ~OMPP_Sampler();
        virtual void run(MTRand& mt);

        int getClassThreads(); 
        int getGenThreads();
        
    private:
        int genThreads;
        int classThreads;
};

#endif /* MCS_H_ */
