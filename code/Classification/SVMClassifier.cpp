/*
 * SVM.cpp
 *
 *  Created on: Feb 7, 2011
 *      Author: rgreen
 */

#include "SVMClassifier.h"
#include <iostream>
using namespace std;

SVMClassifier::SVMClassifier() : Classifier() {
    initialize();
}
SVMClassifier::SVMClassifier(std::string c, int n, std::vector < Generator > g, std::vector < Bus > b) : Classifier(c, n, g, b) {
    initialize();
    classifier = new OPFClassifier(c, n, g, b);
}

SVMClassifier::SVMClassifier(std::string c, int n, std::vector < Generator > g, std::vector < Bus > b, std::vector < Line > l) : Classifier(c, n, g, b, l) {
    initialize();
    classifier = new OPFClassifier(c, n, g, b, l);
}

SVMClassifier::SVMClassifier(std::string c, int n, std::vector < Generator > g, std::vector < Bus > b, std::vector < Line > l, Classifier* cl) : Classifier (c, n, g, b, l){
    initialize();
    classifier = cl;
}
SVMClassifier* SVMClassifier::clone() const { return new SVMClassifier(*this);}

void SVMClassifier::initialize(){
    svmProblem                 = NULL;
    svmParameters             = NULL;
    trained                 = false;
    lineAdjustment             = 1;

    if(curSystem == "RTS96"){ numTrainingStates = 3000;
    }else                   { numTrainingStates = 1000;}
    classifier                 = NULL;
    verbose                 = false;
    validateClassification     = true;
}

SVMClassifier::~SVMClassifier(){
    delete classifier;
    svm_destroy_param(svmParameters);
    free(svmProblem->x);
    free(svmProblem->y);
}
void SVMClassifier::reset(){

}

void SVMClassifier::generateTrainingData(){
    bool contains;
    int solSize;
    double excess, retValue, stateProb, totalCap;
    MTRand mt;
    std::vector<double> curSolution;
    std::vector<double> vInput;
    std::vector<double> vOutput;

    if(useLines && lines.size() > 0){ solSize = gens.size() + lines.size();
    }else                           { solSize = gens.size();}

    curSolution .resize(solSize, 0);
    vInput      .resize(solSize+2, 0);
    vOutput     .resize(1, 0);

    if(classifier == NULL){ 
        classifier = new OPFClassifier(curSystem, nb, gens, buses, lines);
        classifier->addLoad(addedLoad);
    }

    trainingInput.clear(); trainingOutput.clear();
    while(trainingInput.size() < (unsigned int)numTrainingStates){
        stateProb = 1.0; totalCap = 0;

        for(unsigned int i=0; i< gens.size(); i++){
            if(mt.rand() <=  gens[i].getOutageRate()){
                curSolution[i]     = 0;
                stateProb         *= gens[i].getOutageRate();
            }else{
                curSolution[i]     = 1;
                totalCap         += gens[i].getPG()/100;
                stateProb         *= (1-gens[i].getOutageRate());
            }
        }
        if(useLines && lines.size() > 0){
            for(unsigned int i=0; i<lines.size(); i++){
                if(mt.rand() <= (lines[i].getOutageRate() * lineAdjustment)){
                    curSolution[gens.size()+i]     = 0;
                    stateProb                     *= (lines[i].getOutageRate());
                }else{
                    curSolution[gens.size()+i]     = 1;
                    stateProb                     *= (1-lines[i].getOutageRate());
                }
            }
        }
        for(unsigned int i=0; i<curSolution.size(); i++){ vInput[i] = curSolution[i];}
        
        classifier->init();
        retValue = classifier->run(curSolution, excess);
        vInput[gens.size()]     = stateProb;
        vInput[gens.size()+1]     = totalCap;

        if(retValue > 0){ vOutput[0] = 1; } // Fail
        else            { vOutput[0] = 0; } // Success

        contains = false;
        for(unsigned int i=0; i<trainingInput.size(); i++){
            if(trainingInput[i] == vInput){
                contains = true;
                break;
            }
        }
        if(!contains){
            trainingInput.push_back(vInput); trainingOutput.push_back(vOutput);
        }
    }
}

void SVMClassifier::scale(){
    std::vector<double> range(2,0);
    double lower = 0, upper = 1;
    int startIndex, endIndex, curPos;

    #ifdef _MSC_VER
        scaleMin.resize(2, std::numeric_limits<double>::infinity());
        scaleMax.resize(2, -std::numeric_limits<double>::infinity());
    #else
        scaleMin.resize(2, INFINITY);
        scaleMax.resize(2, -INFINITY);
    #endif
    
    // For Each Input
    if(useLines){ startIndex = gens.size()+lines.size();}
    else        { startIndex = gens.size();}

    endIndex = startIndex+2;
    curPos = 0;
    for(int i=startIndex; i<endIndex; i++){
        // For each Training Pattern
        for(unsigned int j=0; j<trainingInput.size(); j++){
            if(trainingInput[j][i] > scaleMax[curPos]) scaleMax[curPos] = trainingInput[j][i];
            if(trainingInput[j][i] < scaleMin[curPos]) scaleMin[curPos] = trainingInput[j][i];
        }
        curPos++;
    }
    //Scale
    range[0] = scaleMax[0]-scaleMin[0];
    range[1] = scaleMax[1]-scaleMin[1];
    curPos = 0;
    for(int i=startIndex; i<endIndex; i++){
        for(unsigned int j=0; j<trainingInput.size(); j++){
            trainingInput[j][i] = lower + (upper-lower) * (trainingInput[j][i]-scaleMin[curPos])/range[curPos];
        }
        curPos++;
    }
}
void SVMClassifier::train(){
    int curIndex = 0;
    char* trainingResult;

    if(svmProblem != NULL){
        free(svmProblem->x);
        free(svmProblem->y);
        delete svmProblem;
    }
    if(svmParameters != NULL){ svm_destroy_param(svmParameters);}

    svmProblem       = new svm_problem();
    svmProblem->l = numTrainingStates;
    svmProblem->y = new double[numTrainingStates];
    svmProblem->x = new svm_node*[numTrainingStates];

    for(int i=0; i<numTrainingStates; i++){
        svmProblem->y[i] = trainingOutput[i][0];
        svmProblem->x[i] = new svm_node[trainingInput[i].size()+1];
    }
    for(int i=0; i<numTrainingStates; i++){
        curIndex = 0;
        for(unsigned int j=0; j<trainingInput[i].size(); j++){
            svmProblem->x[i][j].index = j+1;
            svmProblem->x[i][j].value = trainingInput[i][j];
            curIndex++;
        }
        // Set End of Vector node
        svmProblem->x[i][curIndex].index = -1;
    }
    svmParameters = new svm_parameter();

    // Set up Parameters - Default Values
    // RTS79
    // 500 training states
    if(curSystem == "RTS79"){
        svmParameters->gamma    = 0.001953125;
        svmParameters->C        = 2048;            // For C_SVC, ONE_CLASS, and NU_SVR
    }else{
        svmParameters->gamma    = 0.03125;
        svmParameters->C        = 32.0;            // For C_SVC, ONE_CLASS, and NU_SVR
    }

    svmParameters->svm_type     = C_SVC;          // C_SVC, NU_SVC, ONE_CLASS, EPSILON_SVR, NU_SVR
    svmParameters->kernel_type    = RBF;             // LINEAR, POLY, RBF, SIGMOID
    svmParameters->cache_size   = 100;            // Cache Size in MB
    svmParameters->eps             = 1e-3;            // Stopping Criteria
    svmParameters->shrinking    = 1;

    // Train
    trainingResult = (char*)svm_check_parameter(svmProblem, svmParameters);
    if(trainingResult != NULL){
        printf("Check Parameters: %s\n", trainingResult);
        return;
    }
    svmModel = svm_train(svmProblem, svmParameters);
    trained = true;
    delete trainingResult;
}
void SVMClassifier::init(){
    timer.startTimer();
    generateTrainingData();
    scale();
    train();
    falsePositives = falseNegatives = trueNegatives = truePositives = 0;

    timer.stopTimer();
    trainingTime = timer.getElapsedTime();
}

void SVMClassifier::scaleSolution(std::vector<double>& curSolution){
    double lower=0, upper=1;

    curSolution[curSolution.size()-2] = lower + (upper-lower) * (curSolution[curSolution.size()-2]-scaleMin[0])/(scaleMax[0]-scaleMin[0]);
    curSolution[curSolution.size()-1] = lower + (upper-lower) * (curSolution[curSolution.size()-1]-scaleMin[1])/(scaleMax[1]-scaleMin[1]);
}

double SVMClassifier::classify(std::vector<double> curSolution, double& excess){
    double retValue = 0, lpRetValue;
    svm_node *svmNode;
    
    svmNode = new svm_node[curSolution.size()+1];

    if(!trained){ train();}

    timer.startTimer();
    scaleSolution(curSolution);
    for(unsigned int i=0; i<curSolution.size(); i++){
        svmNode[i].index = i+1;
        svmNode[i].value = curSolution[i];
    }
    svmNode[curSolution.size()].index = -1;
    retValue = svm_predict(svmModel, svmNode);
    
    if(validateClassification){
        classifier->reset();
        lpRetValue = classifier->run(curSolution, excess);
        if(retValue == 0 && lpRetValue == 0){
            truePositives++;
        }
        if(retValue == 0 && lpRetValue != 0){
            falsePositives++;
        }
        if(retValue != 0 && lpRetValue != 0){
            trueNegatives++;
        }
        if(retValue != 0 && lpRetValue == 0){
            falseNegatives++;
        }
    }

    timer.stopTimer();
    classificationTime += timer.getElapsedTime();

    delete [] svmNode;
    return retValue;
}

double SVMClassifier::classify(std::string curSolution, double& excess){
    std::vector<double> newSolution(curSolution.size());

    for(unsigned int i=0; i<curSolution.size(); i++){
        newSolution[i] = atof(&curSolution[i]);
    }
    return classify(newSolution, excess);
}
double SVMClassifier::classify(std::vector<int> curSolution, double& excess){
    std::vector<double> newSolution(curSolution.size(),0);

    for(unsigned int i=0; i<curSolution.size(); i++){
        newSolution[i] = curSolution[i];
    }
    return classify(newSolution, excess);
}

void SVMClassifier::load(){
    if(!trained) train();
}
double SVMClassifier::run(std::vector<double> curSolution, double& excess){
    return classify(curSolution, excess);
}
double SVMClassifier::run(std::string curSolution, double& excess){
    return classify(curSolution, excess);
}
double SVMClassifier::getClassificationTime(){
    return classificationTime;
}
double SVMClassifier::getTrainingTime(){ return trainingTime; }


double SVMClassifier::getSensitivity(){
    double retValue, num, denom;

    num = truePositives;
    denom = truePositives+falseNegatives;
    if(!validateClassification || denom == 0){
        retValue = 0;
    }else{
        retValue = num/denom;
    }
    return retValue;
}
double SVMClassifier::getSpecificity(){
    double retValue, num, denom;

    num = trueNegatives;
    denom = trueNegatives+falsePositives;
    if(!validateClassification || denom == 0){
        retValue = 0;
    }else{
        retValue = num/denom;
    }
    return retValue;
}
double SVMClassifier::getAccuracy(){
    double retValue, num, denom;

    num = truePositives+trueNegatives;
    denom = truePositives+trueNegatives+falsePositives+falseNegatives;
    if(!validateClassification || denom == 0){
        retValue = 0;
    }else{
        retValue = num/denom;
    }
    return retValue;
}
double SVMClassifier::getPrecision(){
    double retValue, num, denom;
    num = truePositives;
    denom = truePositives+falsePositives;

    if(!validateClassification || denom == 0){
        retValue = 0;
    }else{
        retValue = num/denom;
    }
    return retValue;
}
