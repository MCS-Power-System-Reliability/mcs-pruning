/*
 * CommandLineUtils.h
 *
 *  Created on: Feb 25, 2019
 *      Author: jkleink
 */

#ifndef COMMAND_LINE_UTILS_H_
#define COMMAND_LINE_UTILS_H_

#include "Utils.h"
#include <memory>

namespace CommandLineUtils{
    extern void setUsage(unique_ptr<AnyOption>& opt);
    extern void setOptions(unique_ptr<AnyOption>& opt);
};

#endif