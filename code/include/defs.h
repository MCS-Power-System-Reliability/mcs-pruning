/*
 * defs.h
 *
 *  Created on: Feb 21, 2011
 *      Author: rgreen
 */

#ifndef DEFS_H_
#define DEFS_H_

#include <map>
#include <string>

namespace pruning{

    enum PRUNING_METHOD         {PM_NONE, PM_ACO, PM_AIS, PM_GA, PM_MGA, PM_PSO, PM_RPSO, PM_CFO};
    enum CLASSIFICATION_METHOD  {CM_CAP, CM_NN, CM_OPF, CM_SVM};
    enum SAMPLING_METHOD        {SM_ACO, SM_AIS, SM_GA, SM_MGA, SM_PSO, SM_MO_PSO, SM_RPSO,    SM_CFO,     // PIS Methods
                                    SM_MCS, SM_MCS_OMPB, SM_MCS_OMPP,                                   // MCS Methods
                                    SM_LHS, SM_DS,                                                        // LHS Methods
                                    SM_HAL, SM_HAM, SM_FAU, SM_SOBOL,                                    // LDS Methods
                                    SM_PI,
                                    SM_ATV                                                              // Antithetic Variate Sampling
                        
                                };
    enum PHEV_PLACEMENT         {PP_NONE, PP_EVEN_ALL_BUSES, PP_EVEN_LOAD_BUSES, PP_RANDOM_ALL_BUSES, PP_RANDOM_LOAD_BUSES, PP_STATIC, PP_FAIR_DISTRIBUTION};
    enum STOPPING_METHOD        {SM_STATES_SLOPE, SM_STATES_PRUNED, SM_PROB_MAX, SM_PROB_SLOPE, SM_GENERATION};

    // Mzx Prob
    // Prob Slope
    enum PRUNING_OBJECTIVE      {PO_PROBABILITY, PO_CURTAILMENT, PO_EXCESS, PO_COPY, PO_ZERO, PO_TUP};
    enum BUS_LOAD_DIST          {BL_HOURLY, BL_DAILY, BL_WEEKLY, BL_PEAK};
}



#endif /* DEFS_H_ */
