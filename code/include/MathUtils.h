/*
 * MathUtils.h
 *
 *  Created on: Feb 25, 2019
 *      Author: jkleink
 */

#ifndef MATH_UTILS_H_
#define MATH_UTILS_H_

#include "Utils.h"

namespace MathUtils{
    extern double sigMoid(double v);
    extern int factorial(int n);
    extern int combination(int n, int r);
    extern void twoByTwoCholeskyDecomp(std::vector<std::vector<double> >& A);
    extern double unitStep(double X);
    extern std::vector< std::vector<int> > matMult(std::vector< std::vector<int> > A, std::vector< std::vector<int> > B);
    extern double expm (double p, double ak);
    extern double series(int m, int id);
    extern std::vector<double> decToBin(double n, int numDigits);
};

#endif