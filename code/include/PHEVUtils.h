/*
 * PHEVUtils.h
 *
 *  Created on: Feb 25, 2019
 *      Author: jkleink
 */

#ifndef PHEV_UTILS_H_
#define PHEV_UTILS_H_

#include "Utils.h"
#include "MathUtils.h"

namespace PHEVUtils{
    extern void calculatePHEVLoad(
            double penetrationLevel, double rho,
            int totalVehicles, int numBuses,
            std::vector<double>& phevLoad, std::vector<double>& phevGen, MTRand& mt, pruning::PHEV_PLACEMENT PHEV_PLACEMENT = pruning::PP_EVEN_ALL_BUSES);

};

#endif