/*
 * VectorUtils.h
 *
 *  Created on: Feb 25, 2019
 *      Author: jkleink
 */

#ifndef VECTOR_UTILS_H_
#define VECTOR_UTILS_H_

#include "Utils.h"

namespace VectorUtils {

    extern void printVector(std::vector < std::vector < double > >& v, std::string title, int precision = 2);
    extern void printVector(std::vector < std::vector < std::vector < double > > >& v, std::string title, int precision = 2);

};

#endif /* VECTOR_UTILS_H_ */