#ifndef BUS_UTILS_H_
#define BUS_UTILS_H_

#include "Utils.h"
#include <memory>

namespace BusUtils{


    inline std::vector<double> wDist  {86.2, 90, 87.8, 83.4, 88, 84.1, 83.2, 80.6, 74, 73.7, 71.5, 72.7, 70.4, 75, 72.1, 80, 75.4, 83.7, 87, 
                                88, 85.6, 81.1, 90, 88.7, 89.6, 86.1, 75.5, 81.6, 80.1, 88, 72.2, 77.6, 80, 72.9, 72.6, 70.5, 78, 
                                69.5, 72.4, 72.4, 74.3, 74.4, 80, 88.1, 88.5, 90.9, 94, 89, 94.2, 97, 100, 95.2};

    inline std::vector<double> dDist  { 93, 100, 98, 96, 94, 77, 75};

    inline std::map<double, double> hDist {
                                {0.0658195970695971,92}, {0.124084249084249, 91}, {0.175595238095238, 100}, {0.226648351648352, 95}, {0.274038461538462, 65}, 
                                {0.31856684981685,  88}, {0.355769230769231, 66}, {0.392971611721612, 93},  {0.428113553113553, 87}, {0.461767399267399, 94}, 
                                {0.493818681318681, 68}, {0.524038461538461, 85}, {0.553685897435897, 99},  {0.581387362637363, 96}, {0.606684981684982, 97}, 
                                {0.630608974358974, 86}, {0.654532967032967, 74}, {0.677655677655678, 64},  {0.699862637362637, 80}, {0.722069597069597, 70}, 
                                {0.74198717948718,  62}, {0.759753663003663, 72}, {0.776923992673993, 81},  {0.793407509157509, 83}, {0.809891025641026, 73}, 
                                {0.825687728937729, 60}, {0.838508241758242, 63}, {0.851328754578754, 59},  {0.86392032967033,  89}, {0.876511904761905, 75}, 
                                {0.88910347985348,  69}, {0.900092490842491, 58}, {0.906044871794871, 56},  {0.911081501831501, 98}, {0.914973443223443, 67}, 
                                {0.917949633699633, 76}
                            };

    extern double calcBusScalingFactor(pruning::BUS_LOAD_DIST loadDistType, MTRand& mt);
  
} 

#endif