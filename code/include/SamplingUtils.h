/*
 * SamplingUtils.h
 *
 *  Created on: Feb 25, 2019
 *      Author: jkleink
 */

#ifndef SAMPLING_UTILS_H_
#define SAMPLING_UTILS_H_

#include "Utils.h"
#include "MathUtils.h"

namespace SamplingUtils {
    
    extern std::ifstream piFin;
    extern std::vector < std::vector < double > > sobol_points(unsigned N, unsigned D);
    extern double *single_sobol_points(unsigned N, unsigned D);

    extern std::vector < std::vector < double > > faureSampling(int Nd, int Ns);
    extern std::vector<int> changeBase(double num, double base, int numDigits);
    extern std::string changeBase(std::string Base, int number);
    extern double corputBase(double base, double number);

    extern std::vector < std::vector < double > > latinHyperCube_Random(int numVars, int numSamples, MTRand& mt);
    extern std::vector < std::vector < double > > descriptiveSampling_Random(int numVars, int numSamples, MTRand& mt);

    extern std::vector < std::vector < double > > hammersleySampling(int Nd, int Ns);
    extern std::vector < std::vector < double > > haltonSampling(int Nd, int Ns);
    extern std::vector < std::vector < double > > faureSampling(int Nd, int Ns);

    extern double piNumber(int Ns);
    extern std::string toLower(std::string str);
    extern std::string toUpper(std::string str);
    extern std::vector<std::string> permuteCharacters(std::string topermute);



};

#endif /* SAMPLING_UTILS_H_ */