/*
 * SystemUtils.h
 *
 *  Created on: Feb 25, 2019
 *      Author: jkleink
 */

#ifndef SYSTEM_UTILS_H_
#define SYSTEM_UTILS_H_

#include "Utils.h"
#include "StringUtils.h"

namespace SystemUtils{
    extern void loadSystemData(double& pLoad, double& qLoad, int& nb, int& nt, std::string curSystem, std::vector<Generator>& gens, std::vector<Line>& lines, std::vector<Bus>& Buses);
};

#endif