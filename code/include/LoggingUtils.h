/*
 * LoggingUtils.h
 *
 *  Created on: Feb 25, 2019
 *      Author: jkleink
 */

#ifndef LOGGING_UTILS_H_
#define LOGGING_UTILS_H_

#include "Utils.h"

namespace LoggingUtils{
    extern void writeLineOutages(std::string root, std::string curSystem, std::string method, std::string pruningMethod,
            std::string classificationMethod, std::string pruningObj,
            bool useLines, bool multiObj, char* aTime,
            bool useLocalSearch, bool usePHEVs, bool negateFitness, double penetrationLevel, pruning::PHEV_PLACEMENT phevPlacement,
            std::vector < std::vector < int > > lineOutageCounts, std::vector<Line> lines, int numThreads);

    extern void writeGeneratorOutages(std::string root, std::string curSystem, std::string method, std::string pruningMethod,
            std::string classificationMethod, std::string pruningObj, bool useLines, bool multiObj, char* aTime,
            bool useLocalSearch, bool usePHEVs, bool negateFitness, double penetrationLevel, pruning::PHEV_PLACEMENT phevPlacement,
            std::vector < std::vector < int > > genOutageCounts, std::vector<Generator> gens, int numThreads);

    extern std::string getBaseFileName(std::string root, std::string curSystem, std::string samplingMethod, std::string pruningMethod,
            std::string classificationMethod, std::string pruningObj,
            bool useLines, bool multiObj, bool useLocalSearch, bool usePHEVs, bool negateFitness, double penetrationLevel, pruning::PHEV_PLACEMENT phevPlacement,
            int numThreads);
                                
    extern std::string getFileName(std::string root, std::string curSystem, std::string samplingMethod, std::string pruningMethod,
            std::string classificationMethod, std::string pruningObj,
            bool useLines, bool multiObj, char* aTime, bool useLocalSearch, bool usePHEVs, bool negateFitness,
            double penetrationLevel, pruning::PHEV_PLACEMENT phevPlacement,int numThreads);
                    
    //extern char* getTimeStamp();
    extern void getTimeStamp(char* aTime);
    /******************************** End Logging ********************************/
};

#endif