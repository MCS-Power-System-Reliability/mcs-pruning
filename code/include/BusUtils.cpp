#include "BusUtils.h"


namespace BusUtils {
    double calcBusScalingFactor(pruning::BUS_LOAD_DIST loadDistType, MTRand& mt){

        using  BusUtils::wDist;
        using BusUtils::dDist;
        using BusUtils::hDist;

        double retValue = 1.0;

        switch(loadDistType){
            case pruning::BL_PEAK:      retValue = 1.0; break;
            case pruning::BL_DAILY:     retValue = dDist[mt.randInt(dDist.size()-1)]/100.00; break;
            case pruning::BL_WEEKLY:    retValue = wDist[mt.randInt(wDist.size()-1)]/100.00; break;
            
            case pruning::BL_HOURLY:    
                double rValue = mt.rand();
                double prevValue = 0;

                for (const auto& [key, value] : hDist) {
                    prevValue = value;

                    if(key > rValue){
                        retValue = prevValue/100.00;
                        break;
                    }
                }
                break;
        }

        return retValue;
    }

};