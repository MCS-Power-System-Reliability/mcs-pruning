/*
 * StringUtils.h
 *
 *  Created on: Feb 25, 2019
 *      Author: jkleink
 */

#ifndef STRING_UTILS_H_
#define STRING_UTILS_H_

#include "Utils.h"

namespace StringUtils{
    extern std::string vectorToString(std::vector<double> v);
    extern std::string vectorToString(std::vector<int> v);
    extern std::string arrayToString(int* v, int size);
    extern void tokenizeString(std::string str,std::vector<std::string>& tokens,const std::string& delimiter );
};

#endif