/*
 * Antibody.h
 *
 *  Created on: Feb 24, 2011
 *      Author: rgreen
 */

#ifndef ANTIBODY_H_
#define ANTIBODY_H_

#include <vector>

class Antibody {
    public:
        Antibody();
        virtual ~Antibody();

        std::vector<int> pos;
        long double      fitness;
        long double      affinity;
};

#endif /* ANTIBODY_H_ */
