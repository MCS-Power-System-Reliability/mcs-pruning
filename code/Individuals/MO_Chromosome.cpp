/*
 * MO_Chromosome.cpp
 *
 *  Created on: Mar 29, 2011
 *      Author: rgreen
 */

#include "MO_Chromosome.h"

MO_Chromosome::MO_Chromosome() {
    // TODO Auto-generated constructor stub
}
MO_Chromosome::MO_Chromosome(int size) {
    pos.resize(size, 0);
}

MO_Chromosome::~MO_Chromosome() {
    // TODO Auto-generated destructor stub
}

std::string MO_Chromosome::toString(){
    std::string s = "";
    for(int i=0; i<(int)pos.size(); i++){
        if(pos[i] == 1){s += "1";}
        else{s += "0";    }
    }
    return s;
}
