/*
 * binaryParticle.h
 *
 *  Created on: Nov 11, 2010
 *      Author: rgreen
 */

#ifndef BINARYPARTICLE_H_
#define BINARYPARTICLE_H_

#include <vector>

class binaryParticle {
    public:
        binaryParticle();
        virtual ~binaryParticle();

         std::vector<int>      pos;
         std::vector<int>      pBest;
         std::vector<double>   vel;
         double                pBestValue;
         double                fitness;
};

#endif /* BINARYPARTICLE_H_ */
