/*
 * MO_ParetoBinaryParticle.h
 *
 *  Created on: Aug 22, 2011
 *      Author: Rob
 */

#ifndef MO_PARETOBINARYPARTICLE_H_
#define MO_PARETOBINARYPARTICLE_H_

#include <vector>

class MO_ParetoBinaryParticle {
    public:
        MO_ParetoBinaryParticle();
        virtual ~MO_ParetoBinaryParticle();

        std::vector < int >      pos;
        std::vector < double >   vel;

        // Holds Multiple Objectives
        std::vector < double >     fitness;
        std::vector < int >        pBest;
        std::vector < double >             pBestValues;

        int leader;
};

#endif /* MO_PARETOBINARYPARTICLE_H_ */
