/*
 * Individual.h
 *
 *  Created on: May 24, 2011
 *      Author: rgreen
 */

#ifndef INDIVIDUAL_H_
#define INDIVIDUAL_H_

#include <vector>

class Individual {
    public:
        Individual();
        virtual ~Individual();

        std::vector<int>    pos;
        std::vector<int>    pBest;
        long double         fitness;
};

#endif /* INDIVIDUAL_H_ */
