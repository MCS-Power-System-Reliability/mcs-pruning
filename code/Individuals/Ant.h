/*
 * Ant.h
 *
 *  Created on: Nov 11, 2010
 *      Author: rgreen
 */

#ifndef ANT_H_
#define ANT_H_

#include <vector>

#include "Individual.h"

class Ant : public Individual {
    public:
        Ant();
        virtual ~Ant();
        int             startingPos;
        int                origStartingPos;
};

#endif /* ANT_H_ */
