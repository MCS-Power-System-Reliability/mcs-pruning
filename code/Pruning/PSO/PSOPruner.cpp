/*
 * PSOPruner.cpp
 *
 *  Created on: Nov 11, 2010
 *      Author: rgreen
 */

#include <map>
#include <vector>

#include "PSOPruner.h"

#include <iostream>

PSOPruner::PSOPruner(int popSize, int generations, shared_ptr<Classifier>& o, std::vector<Generator> g, std::vector<Line> l, double p, bool ul)
         : Pruner(popSize, generations, g, l, o, p, ul){
         
    if(l.size() > 0 && useLines){
        Init(popSize, generations, -2.38357, 1.77067, 2.52124 , o, g, l, p, ul);
    }else{
        Init(popSize, generations, -3.05101, 1.70618, 2.0176, o, g, l, p);
    }
}

PSOPruner::PSOPruner(int popSize, int generations, double c1, double c2, double w, shared_ptr<Classifier>& o, std::vector<Generator> g, std::vector<Line> l, double p, bool ul)
         : Pruner(popSize, generations, g, l, o, p, ul){
    Init(popSize, generations, c1, c2, w, o, g, l, p, ul);
}

void PSOPruner::Init(int popSize, int generations, double c1, double c2, double w, shared_ptr<Classifier>& o, std::vector<Generator> g, std::vector<Line> l, double p, bool ul){
    Pruner::Init(popSize, generations, g, l, o, p, ul);

    C1	= c1;
    C2	= c2;
    W	= w;

    initTime    = 0;
    fitnessTime = 0;
    updateTime  = 0;
}

void PSOPruner::Reset(int np, int nt){
    Pruner::Reset(np, nt);

    initTime    = 0;
    fitnessTime = 0;
    updateTime  = 0;

    clearTimes();
    clearVectors();
}

void PSOPruner::clearVectors(){
    Pruner::clearVectors();
    swarm.clear();
}

void PSOPruner::clearTimes(){
    Pruner::clearTimes();

    initTime    = 0;
    fitnessTime = 0;
    updateTime  = 0;
}
PSOPruner::~PSOPruner() {
    // TODO Auto-generated destructor stub
}
void PSOPruner::initPopulation(MTRand& mt){

    swarm.clear();

    subTimer.startTimer();
    for(int i=0; i<Np; i++){
        binaryParticle p;

        for(int j=0; j<(int)gens.size(); j++){
            p.pos.push_back((mt.rand() < gens[j].getOutageRate() ? 0 : 1 ));
            p.vel.push_back(gens[j].getOutageRate());
            p.pBest.push_back(0);
        }

        if(useLines){
            for(int j=0; j<(int)lines.size(); j++){
                p.pos.push_back((mt.rand() < lines[j].getOutageRate() ? 0 : 1 ));
                p.vel.push_back(lines[j].getOutageRate());
                p.pBest.push_back(0);
            }
        }

        #ifdef _MSC_VER
            p.pBestValue = -std::numeric_limits<double>::infinity();
        #else
            p.pBestValue = -INFINITY;
        #endif
        swarm.push_back(p);
    }
    subTimer.stopTimer();
    initTime += subTimer.getElapsedTime();
}
void PSOPruner::evaluateFitness(){
    
    subTimer.startTimer();
  
    for(unsigned int i=0; i<swarm.size(); i++){

        swarm[i].fitness = EvaluateSolution(swarm[i].pos);
        //Local Best
        if(swarm[i].fitness > swarm[i].pBestValue){
            swarm[i].pBest       = swarm[i].pos;
            swarm[i].pBestValue =  swarm[i].fitness;
        }
    }
    for(unsigned int i=0; i<swarm.size(); i++){
        if(swarm[i].fitness > gBestValue){
            gBest      = swarm[i].pos;
            gBestValue = swarm[i].fitness;
        }
    }
    subTimer.stopTimer();
    fitnessTime += subTimer.getElapsedTime();
}
void PSOPruner::updatePositions(MTRand& mt){
    double R1, R2, newV;

    subTimer.startTimer();
    for(unsigned int i=0; i< swarm.size(); i++){
        for(int j=0; j<Nd; j++){
            R1 = mt.rand();
            R2 = mt.rand();

            newV =  W * swarm[i].vel[j];
            newV += C1 * R1 * (swarm[i].pBest[j] - swarm[i].pos[j]);
            newV += C2 * R2 * (gBest[j] - swarm[i].pos[j]);
            newV = sigMoid(newV);
            swarm[i].vel[j] = newV;

            if(newV > mt.rand()){
                swarm[i].pos[j] = 1;
            }else{
                swarm[i].pos[j] = 0;
            }
        }
    }
    subTimer.stopTimer();
    updateTime += subTimer.getElapsedTime();
}
void PSOPruner::Prune(MTRand& mt){

    timer.startTimer();
    gBest.resize(gens.size() + lines.size(), 0.0);
    
    #ifdef _MSC_VER
        gBestValue = -std::numeric_limits<double>::infinity();
    #else
        gBestValue = -INFINITY;
    #endif

    initPopulation(mt);
    totalIterations = 0;
    while(!isConverged()){
        evaluateFitness();
        updatePositions(mt);
        totalIterations++;
    }
    timer.stopTimer();
    pruningTime = timer.getElapsedTime();

    //cout << setprecision(6) << initTime << " " << fitnessTime << " " << updateTime << "\n";
}

double PSOPruner::sigMoid(double v){
    return 1/(1+exp(-v));
}

bool PSOPruner::isConverged() {
    bool retValue = Pruner::isConverged();
    double totalHammingDist = 0, localHammingDist = 0;
    double avgFitness = 0, stdDev = 0;

    if(useLogging && totalIterations > 1){
        for(unsigned int i=0; i<swarm.size(); i++){
            avgFitness += swarm[i].fitness;
        }
        avgFitness /= ((double)swarm.size());

        for(unsigned int i=0; i<swarm.size(); i++){
            stdDev += pow(swarm[i].fitness - avgFitness,2);
        }
        stdDev /= (swarm.size()-1.0);
        stdDev = sqrt(stdDev);
        stdDevFitness.push_back(stdDev);

        totalHammingDist = 0;
        for(unsigned int i=0; i<swarm.size(); i++){
            for(unsigned int j=0; j<swarm.size(); j++){
                if(i != j){
                    localHammingDist = 0;
                    for(unsigned int k=0; k<swarm[i].pos.size(); k++){
                        localHammingDist +=  abs(swarm[i].pos[k] - swarm[j].pos[k]);
                    }
                    totalHammingDist += localHammingDist;
                }
            }
        }
        hammingDiversity.push_back(totalHammingDist/swarm.size());
    }
    return retValue;
}

void PSOPruner::setC1(double i)  { C1 = i;}
void PSOPruner::setC2(double i)  { C2 = i;}
void PSOPruner::setW(double i)   { W = i;}