/*
 * PSOPruner.h
 *
 *  Created on: Nov 11, 2010
 *      Author: rgreen
 */

#ifndef PSOPRUNER_H_
#define PSOPRUNER_H_

#include <vector>
#include <map>

#include "Pruner.h"
#include "binaryParticle.h"

class PSOPruner : public Pruner {
    public:
        PSOPruner(int popSize, int generations, double c1, double c2, double w, shared_ptr<Classifier>& lp, std::vector<Generator> g, std::vector<Line> l, double pLoad, bool ul=false);
        PSOPruner(int popSize, int generations, shared_ptr<Classifier>& lp, std::vector<Generator> g, std::vector<Line> l, double pLoad, bool ul=false);
        void Init(int popSize, int generations, double c1, double c2, double w, shared_ptr<Classifier>& lp, std::vector<Generator> g, std::vector<Line> l,
                    double pLoad, bool ul=false);

        void Reset(int np, int nt);

        virtual ~PSOPruner();

        virtual void Prune(MTRand& mt);
        
        void setC1(double i);
        void setC2(double i);
        void setW(double i);    

    protected:

        bool    isConverged();
        void 	initPopulation(MTRand& mt);
        void 	evaluateFitness();
        void 	updatePositions(MTRand& mt);
        void 	clearVectors();
        void 	clearTimes();
        double 	sigMoid(double v);

        int iterations;

        double gBestValue;
        double initTime, fitnessTime, updateTime;
        double C1;
        double C2;
        double W;
        
        std::vector<binaryParticle> swarm;
        std::vector<int> gBest;
};

#endif /* PSOPRUNER_H_ */
