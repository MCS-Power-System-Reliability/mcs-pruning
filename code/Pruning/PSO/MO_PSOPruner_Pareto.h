/*
 * MO_PSOPruner_Pareto.h
 *
 *  Created on: Aug 16, 2011
 *      Author: Rob
 */

#ifndef MO_PSOPRUNER_PARETO_H_
#define MO_PSOPRUNER_PARETO_H_

#include "MO_PSOPruner.h"
//#include "MO_BinaryParticle.h"
#include "MO_ParetoBinaryParticle.h"

class MO_PSOPruner_Pareto : public MO_PSOPruner{
	public:
		MO_PSOPruner_Pareto(int popSize, int generations, shared_ptr<Classifier>& lp, std::vector<Generator> g, std::vector<Line> l, double pLoad, bool ul=false);
		virtual ~MO_PSOPruner_Pareto();

		void printArchive();
	private:
		void evaluateFitness(MTRand& mt);
		void initPopulation(MTRand& mt);
		void Prune(MTRand& mt);
		void updateArchive(MTRand& mt);
		void updatePositions(MTRand& mt);
		void chooseLeader(MTRand& mt);

		std::vector < std::vector < double > > archivedFitness;
		std::vector < std::vector < int > > archivedPosition;

		std::vector <MO_ParetoBinaryParticle> swarm;
};

#endif /* MO_PSOPRUNER_PARETO_H_ */
