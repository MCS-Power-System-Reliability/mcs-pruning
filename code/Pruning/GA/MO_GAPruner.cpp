/*
 * MO_GAPruner.cpp
 *
 *  Created on: Mar 29, 2011
 *      Author: rgreen
 */

#include "MO_GAPruner.h"

MO_GAPruner::MO_GAPruner(int popSize, int generations, shared_ptr<Classifier>& o, std::vector<Generator> g, std::vector<Line> l, double p, bool ul)
    : MOPruner(popSize, generations, g, l, o, p, ul)
{
    Init(popSize, generations, .9, .1, o, g, l, p, ul);
}

MO_GAPruner::MO_GAPruner(int popSize, int generations, double mut, double cross, shared_ptr<Classifier>& o, std::vector<Generator> g, std::vector<Line> l, double p, bool ul)
        : MOPruner(popSize, generations, g, l, o, p, ul)
{
    Init(popSize, generations, mut, cross, o, g, l, p, ul);
}
void MO_GAPruner::Init(int popSize, int generations, double mut, double cross, shared_ptr<Classifier>& o, std::vector<Generator> g, std::vector<Line> l, double p, bool ul){

    MOPruner::Init(popSize, generations, g, l, o, p, ul);
    pMut         = mut;
    pCrossover     = cross;
}
MO_GAPruner::~MO_GAPruner() {
}


void MO_GAPruner::Reset(int np, int nt) {
    MOPruner::Reset(np, nt);

    clearVectors();
    clearTimes();
}

void MO_GAPruner::clearTimes(){
    MOPruner::clearTimes();

    initTime    = 0;
    fitnessTime = 0;
    selectTime    = 0;
    crossTime    = 0;
    mutTime        = 0;
}

void MO_GAPruner::clearVectors(){
    MOPruner::clearVectors();

    pop.clear();
    newPop.clear();
    toCross.clear();
}

bool MO_GAPruner::sortVector(const std::vector<double>& elem1, const std::vector<double>& elem2){
    return elem1[1] < elem2[1];
}

bool MO_GAPruner::sortPop(const MO_Chromosome& elem1, const MO_Chromosome elem2){
    return elem1.fitness < elem2.fitness;
}
void MO_GAPruner::initPopulation(MTRand& mt){
    timer1.startTimer();
    for(unsigned int p=0; p<pop.size(); p++){
        for(unsigned int i=0; i<gens.size(); i++){
            if(mt.rand() < gens[i].getOutageRate()){
                pop[p].pos.push_back(0);
            }else{
                pop[p].pos.push_back(1);
            }
        }
        if(useLines){
            for(unsigned int i=0; i<lines.size(); i++){
                if(mt.rand() < lines[i].getOutageRate()){
                    pop[p].pos.push_back(0);
                }else{
                    pop[p].pos.push_back(1);
                }
            }
        }
    }
    timer1.stopTimer();
    initTime = timer1.getElapsedTime();
}
void MO_GAPruner::evaluateFitness(){
    timer1.startTimer();
    totalFitness = 0;

    //TODO
    /*for(int p=0; p<Np; p++){
        pop[p].fitness = EvaluateSolution(pop[p].pos);
        if(pop[p].fitness > bestFitness){
            bestFitness = pop[p].fitness;
        }
        totalFitness += pop[p].fitness;
    }
    std::sort(pop.begin(), pop.end(), sortPop);

    totalFitness = 0;
    for(int p=0; p<Np; p++){
        pop[p].fitness     =    p+1;
        totalFitness     += pop[p].fitness;
    }
    */
    timer1.stopTimer();
    fitnessTime += timer1.getElapsedTime();
}
void MO_GAPruner::selectionAndCrossover(MTRand& mt){
    int cIndex;

    newPop.clear();
    while((int)newPop.size() < Np * 2){

        // Selection
        timer1.startTimer();
        toCross.clear();
        while(toCross.size() < 2){
            double cIndex = mt.rand(totalFitness);
            double sum = 0;
            for(int p=0; p<(int)pop.size(); p++){
                //TODO
                //sum += pop[p].fitness;
                if(sum > cIndex){
                    toCross.push_back(pop[p].pos);
                    break;
                }
            }
        }
        timer1.stopTimer();
        selectTime += timer1.getElapsedTime();

        // Crossover
        timer1.startTimer();
        MO_Chromosome g1(Nd); MO_Chromosome g2(Nd);
        cIndex = mt.randInt(gens.size());
        for(int i=0; i< Nd; i++){
            if(i < cIndex){
                g1.pos[i] = toCross[0][i];
                g2.pos[i] = toCross[1][i];
            }else{
                g1.pos[i] =  toCross[1][i];
                g2.pos[i] =  toCross[0][i];
            }
        }
        newPop.push_back(g1);
        newPop.push_back(g2);
        timer1.stopTimer();
        crossTime += timer1.getElapsedTime();
    }

    sort(newPop.begin(), newPop.end(), sortPop);
    pop.clear();
    for(int p=0; p<Np; p++){
        pop.push_back(newPop[p]);
    }
}

void MO_GAPruner::mutate(MTRand& mt){
    timer1.startTimer();
    std::string curSolution;

    for(int p=0; p<(int)pop.size();p++){
        if(mt.rand() < pMut){
            //do{
                curSolution = "";
                for(int i=0; i<(int)gens.size(); i++){
                    if(mt.rand() < gens[i].getOutageRate()){
                        pop[p].pos[i] = 0;
                        curSolution     += "0";
                    }else{
                        pop[p].pos[i] = 1;
                        curSolution     += "1";
                    }
                }
                if(useLines){
                    cout << "Lines\n";
                    for(int i=0; i<(int)lines.size(); i++){
                        if(mt.rand() < lines[i].getOutageRate()){
                            pop[p].pos[gens.size() + i] = 0;
                            curSolution     += "0";
                        }else{
                            pop[p].pos[gens.size() + i] = 1;
                            curSolution     += "1";
                        }
                    }
                }
            //}while(successStates.find(curSolution) != successStates.end() || failureStates.find(curSolution) != failureStates.end());
        }
    }
    timer1.stopTimer();
    mutTime += timer1.getElapsedTime();
}

void MO_GAPruner::Prune(MTRand& mt){
    timer.startTimer();
    initTime    = 0, fitnessTime = 0,
    selectTime    = 0, crossTime    = 0,
    mutTime        = 0;

    pop.resize(Np, 0.0);
    #ifdef _MSC_VER
        bestFitness = -std::numeric_limits<double>::infinity();
    #else
        bestFitness = -INFINITY;
    #endif
    initPopulation(mt);

    totalIterations = 0;
    while(!isConverged()){
        evaluateFitness();
        selectionAndCrossover(mt);
        mutate(mt);
        totalIterations++;
    }

    timer.stopTimer();
    pruningTime = timer.getElapsedTime();
}

bool MO_GAPruner::isConverged() {
    bool retValue = Pruner::isConverged();
    double avgHammingDist = 0, localHammingDist = 0;
    double avgFitness = 0, stdDev = 0;

    if(useLogging && totalIterations > 1){
        for(unsigned int i=0; i<pop.size(); i++){
            //TODO
            //avgFitness += pop[i].fitness;
        }
        avgFitness /= ((double)pop.size());

        for(unsigned int i=0; i<pop.size(); i++){
            //TODO
            //stdDev += pow(pop[i].fitness - avgFitness,2);
        }
        stdDev = stdDev / (((double)pop.size())-1.0);
        stdDev = sqrt(stdDev);
        stdDevFitness.push_back(stdDev);

        localHammingDist = 0; avgHammingDist = 0;
        for(unsigned int i=0; i<pop.size(); i++){
            for(unsigned int j=0; j<pop.size(); j++){
                if(i != j){
                    localHammingDist = 0;
                    for(unsigned int k=0; k<pop[i].pos.size(); k++){
                        localHammingDist +=  abs(pop[i].pos[k] - pop[i].pos[j]);
                    }
                    avgHammingDist += localHammingDist;
                }
            }
        }
        hammingDiversity.push_back(avgHammingDist/pop.size());
    }
//    if(retValue){
//            std::map<std::string, double>::iterator p;
//
//              for(p = successStates.begin(); p != successStates.end(); p++) {
//                cout << p->first << " " << p->second << endl;
//              }
//
//        }
    return retValue;
}

