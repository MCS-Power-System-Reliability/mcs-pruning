/*
 * GAPruner.cpp
 *
 *  Created on: Nov 11, 2010
 *      Author: rgreen
 */

#include "GAPruner.h"

GAPruner::GAPruner(int popSize, int generations, shared_ptr<Classifier>& o, std::vector<Generator> g, std::vector<Line> l, double p, bool ul)
    : Pruner(popSize, generations, g, l, o, p, ul)
{
    Init(popSize, generations, .9, .1, o, g, l, p, ul);
}
GAPruner::GAPruner(int popSize, int generations, double mut, double cross, shared_ptr<Classifier>& o, std::vector<Generator> g, std::vector<Line> l, double p, bool ul)
        : Pruner(popSize, generations, g, l, o, p, ul)
{
    Init(popSize, generations, mut, cross, o, g, l, p, ul);
}
void GAPruner::Init(int popSize, int generations, double mut, double cross, shared_ptr<Classifier>& o, std::vector<Generator> g, std::vector<Line> l, double p, bool ul){

    Pruner::Init(popSize, generations, g, l, o, p, ul);
    pMut         = mut;
    pCrossover     = cross;
}
GAPruner::~GAPruner() {
}
void GAPruner::Reset(int np, int nt) {
    Pruner::Reset(np, nt);

    clearVectors();
    clearTimes();
}
void GAPruner::clearTimes(){
    Pruner::clearTimes();

    initTime    = 0;
    fitnessTime = 0;
    selectTime    = 0;
    crossTime    = 0;
    mutTime        = 0;
}
void GAPruner::clearVectors(){
    Pruner::clearVectors();

    pop.clear();
}
bool GAPruner::sortVector(const std::vector<double>& elem1, const std::vector<double>& elem2){
    return elem1[1] < elem2[1];
}
bool GAPruner::sortPop(const Chromosome& elem1, const Chromosome elem2){
    return elem1.fitness < elem2.fitness;
}
void GAPruner::initPopulation(MTRand& mt){
    subTimer.startTimer();
    
    for(unsigned int p=0; p<pop.size(); p++){

        for(unsigned int i=0; i<gens.size(); i++){
            if(mt.rand() < gens[i].getOutageRate()) { pop[p].pos.push_back(0);}
            else                                    { pop[p].pos.push_back(1);
            }
        }
        if(useLines && lines.size() > 0){
            for(unsigned int i=0; i<lines.size(); i++){
                if(mt.rand() < lines[i].getOutageRate()) { pop[p].pos.push_back(0);}
                else                                     { pop[p].pos.push_back(1);}
            }
        }
    }
    subTimer.stopTimer();
    initTime = subTimer.getElapsedTime();
}
void GAPruner::evaluateFitness(){
    totalFitness = 0;

    subTimer.startTimer();

    for(int p=0; p<Np; p++){
        pop[p].fitness = EvaluateSolution(pop[p].pos);    
    }
    std::sort(pop.begin(), pop.end(), sortPop);
    
    for(int p=0; p<Np; p++){
        pop[p].fitness     =    p+1;
        totalFitness     += pop[p].fitness;
        
        if(pop[p].fitness >  bestFitness){
            bestFitness = pop[p].fitness;
        }
    }
    subTimer.stopTimer();
    fitnessTime += subTimer.getElapsedTime();
}
void GAPruner::selectionAndCrossover(MTRand& mt){

    int cIndex;
    double sum;
    std::vector <Chromosome> newPop;
    std::vector < std::vector < int > > toCross;
    
    newPop.clear();

    while((int)newPop.size() < Np * 2){

        // Selection
        subTimer.startTimer();
        toCross.clear();
        while(toCross.size() < 2){
            cIndex = mt.rand(totalFitness);
            sum = 0;
            for(int p=0; p<(int)pop.size(); p++){
                sum += pop[p].fitness;
                if(sum > cIndex){
                    toCross.push_back(pop[p].pos);
                    break;
                }
            }
        }
        subTimer.stopTimer();
        selectTime += subTimer.getElapsedTime();

        // Crossover
        subTimer.startTimer();
        Chromosome g1(Nd); Chromosome g2(Nd);
        cIndex = mt.randInt(gens.size());
        for(int i=0; i< Nd; i++){
            if(i < cIndex){
                g1.pos[i] = toCross[0][i];
                g2.pos[i] = toCross[1][i];
            }else{
                g1.pos[i] =  toCross[1][i];
                g2.pos[i] =  toCross[0][i];
            }
        }
        g1.fitness = EvaluateSolution(g1.pos);
        g2.fitness = EvaluateSolution(g2.pos);
        newPop.push_back(g1);
        newPop.push_back(g2);
        subTimer.stopTimer();
        crossTime += subTimer.getElapsedTime();
    }

    sort(newPop.begin(), newPop.end(), sortPop);
    pop.clear();
    for(int p=0; p<Np; p++){
        pop.push_back(newPop[p]);
    }

    toCross.clear(); newPop.clear();
}
void GAPruner::mutate(MTRand& mt){
    subTimer.startTimer();

    for(int p=0; p<(int)pop.size();p++){
        if(mt.rand() < pMut){
            for(int i=0; i<(int)gens.size(); i++){
                if(mt.rand() < gens[i].getOutageRate()) { pop[p].pos[i] = 0;}
                else                                    { pop[p].pos[i] = 1;}
            }
            if(useLines){
                for(int i=0; i<(int)lines.size(); i++){
                    if(mt.rand() < lines[i].getOutageRate()){ pop[p].pos[gens.size() + i] = 0;}
                    else                                    { pop[p].pos[gens.size() + i] = 1;}
                }
            }
        }
    }
    subTimer.stopTimer();
    mutTime += subTimer.getElapsedTime();
}
void GAPruner::Prune(MTRand& mt){
    timer.startTimer();
    initTime    = 0, fitnessTime = 0,
    selectTime    = 0, crossTime    = 0,
    mutTime        = 0;

    pop.resize(Np, 0);
    #ifdef _MSC_VER
        bestFitness = -std::numeric_limits<double>::infinity();
    #else
        bestFitness = -INFINITY;
    #endif
    initPopulation(mt);

    totalIterations = 0;
    while(!isConverged()){
        evaluateFitness();
        selectionAndCrossover(mt);
        mutate(mt);

        totalIterations++;
    }

    timer.stopTimer();
    pruningTime = timer.getElapsedTime();

    /*cout << setprecision(6)  
         << initTime     << " " 
         << fitnessTime << " "
         << selectTime     << " "
         << crossTime   << " "
         << mutTime     << "\n";*/
}
bool GAPruner::isConverged() {
    bool retValue = Pruner::isConverged();
    double avgHammingDist = 0, localHammingDist = 0;
    double avgFitness = 0, stdDev = 0;

    if(useLogging && totalIterations > 1){
        for(unsigned int i=0; i<pop.size(); i++){
            avgFitness += pop[i].fitness;
        }
        avgFitness /= ((double)pop.size());

        for(unsigned int i=0; i<pop.size(); i++){
            stdDev += pow(pop[i].fitness - avgFitness,2);
        }
        stdDev = stdDev / (((double)pop.size())-1.0);
        stdDev = sqrt(stdDev);
        stdDevFitness.push_back(stdDev);

        localHammingDist = 0; avgHammingDist = 0;
        for(unsigned int i=0; i<pop.size(); i++){
            for(unsigned int j=0; j<pop.size(); j++){
                if(i != j){
                    localHammingDist = 0;
                    for(unsigned int k=0; k<pop[i].pos.size(); k++){
                        localHammingDist +=  abs(pop[i].pos[k] - pop[j].pos[k]);
                    }
                    avgHammingDist += localHammingDist;
                }
            }
        }
        hammingDiversity.push_back(avgHammingDist/pop.size());
    }
//    if(retValue){
//            std::map<std::string, double>::iterator p;
//
//              for(p = successStates.begin(); p != successStates.end(); p++) {
//                cout << p->first << " " << p->second << endl;
//              }
//
//        }
    return retValue;
}
void GAPruner::setMutationRate(double mr)    { if(mr >= 0.0 && mr <= 1.0) {pMut = mr;}}
void GAPruner::setCrossOverRate(double cr)    { if(cr >= 0.0 && cr <= 1.0) {pCrossover = cr;}}
