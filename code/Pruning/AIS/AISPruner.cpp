/*
 * AISPruner.cpp
 *
 *  Created on: Feb 24, 2011
 *      Author: rgreen
 */

#include "AISPruner.h"

AISPruner::AISPruner(int popSize, int generations, shared_ptr<Classifier>& o, std::vector<Generator> g, std::vector<Line> l, double _pLoad,
        bool ul, double _cloneFactor, double _pMut)
         : Pruner(popSize, generations, g, l, o, _pLoad, ul){

   Init(popSize, generations, o, g, l, _pLoad,  ul, _cloneFactor, _pMut);
}
void AISPruner::Init(int popSize, int generations, shared_ptr<Classifier>& o, std::vector<Generator> g, std::vector<Line> l, double _pLoad,
                    bool ul, double _cloneFactor, double _pMut){
    Pruner::Init(popSize, generations, g, l, o, _pLoad, ul);

    pMut		= _pMut;
    cloneFactor	= _cloneFactor;
    initTime    = 0; fitnessTime = 0;
    cloneTime   = 0; sortTime    = 0;
}

void AISPruner::Reset(int np, int nt){
    Pruner::Reset(np, nt);
    clearTimes();
    clearVectors();
}

void AISPruner::clearVectors(){
    Pruner::clearVectors();
    antibodies.clear();
}
void AISPruner::clearTimes(){
    Pruner::clearTimes();
    
    initTime     = 0; 
    fitnessTime  = 0;
    cloneTime    = 0;
    sortTime     = 0;
    affinityTime = 0;
}
AISPruner::~AISPruner() {
    // TODO Auto-generated destructor stub
}
bool AISPruner::sortPop(const Antibody& elem1, const Antibody& elem2){
    return elem1.fitness < elem2.fitness;
}
void AISPruner::initPopulation(MTRand& mt){

    #ifdef _MSC_VER
        gBestValue = -std::numeric_limits<double>::infinity();
    #else
        gBestValue = -INFINITY;
    #endif
    antibodies.clear(); clones.clear();

    subTimer.startTimer();
    for(int i=0; i<Np; i++){
        Antibody p;

        for(int j=0; j<(int)gens.size(); j++){
            p.pos.push_back((mt.rand() < gens[j].getOutageRate() ? 0 : 1 ));
        }
        if(useLines){
            for(int j=0; j<(int)lines.size(); j++){
                p.pos.push_back((mt.rand() < lines[j].getOutageRate() ? 0 : 1 ));
            }
        }

        antibodies.push_back(p);
    }
    subTimer.stopTimer();
    initTime += subTimer.getElapsedTime();
}
void AISPruner::evaluateFitness(){

    for(unsigned int i=0; i<antibodies.size(); i++){
        antibodies[i].fitness = EvaluateSolution(antibodies[i].pos);
    }
    for(unsigned int i=0; i<antibodies.size(); i++){
        if(antibodies[i].fitness > gBestValue){
            gBestValue  = antibodies[i].fitness;
        }
    }
    subTimer.stopTimer();
    fitnessTime += subTimer.getElapsedTime();
}

void AISPruner::cloneAntibodies(MTRand& mt){
    double mutRate;
    int    numClones;
    bool   mutated;
   
    clones.clear();

    subTimer.startTimer();
    calculateAffinity();

    for(int p=0; p<Np; p++){
        mutRate = exp(pMut * antibodies[p].affinity);
        numClones = floor(cloneFactor*Np);

        for(int c=0; c<numClones; c++){
            Antibody a = antibodies[p];

            mutated = false;
            for(int i=0; i<(int)gens.size(); i++){
                if(mt.rand() < mutRate){
                    mutated = true;
                    if(mt.rand() < gens[i].getOutageRate()) { a.pos[i] = 0;}
                    else                                    { a.pos[i] = 1;}
                }
            }
            if(useLines && lines.size() > 0){
                for(int i=0; i<(int)lines.size(); i++){
                    if(mt.rand() < mutRate){
                        mutated = true;
                        if(mt.rand() < lines[i].getOutageRate()){ a.pos[gens.size()+i] = 0;}
                        else                                    { a.pos[gens.size()+i] = 1;}
                    }
                }
            }
            if(mutated) { a.fitness = EvaluateSolution(a.pos);}
            else        { a.fitness = antibodies[p].fitness;}
            clones.push_back(a);
        }
    }
    subTimer.stopTimer();
    cloneTime += subTimer.getElapsedTime();
}
void AISPruner::calculateAffinity(){
    double range;

    subTimer.startTimer();
    sort(antibodies.begin(), antibodies.end(), sortPop);
    range = antibodies[0].fitness - antibodies[Np-1].fitness;

    if(range == 0){
        for(int p=0; p<Np; p++){
            antibodies[p].affinity = 1.0;
        }
    }else{
        for(int p=0; p<Np; p++){
            antibodies[p].affinity = 1.0 - (antibodies[p].fitness/range);
        }
    }
    subTimer.stopTimer();
    affinityTime += subTimer.getElapsedTime();
}

void AISPruner::addClonesToPopulation(){
    
    subTimer.startTimer();
    for(unsigned int i=0; i<clones.size(); i++){
        antibodies.push_back(clones[i]);
    }
    clones.clear();
    
    subTimer.stopTimer();
    cloneTime += subTimer.getElapsedTime();
}
void AISPruner::sortAndTruncate(){

    subTimer.startTimer();
    std::sort(antibodies.begin(), antibodies.end(), sortPop);
    antibodies.resize(Np);
    subTimer.stopTimer();
    sortTime += subTimer.getElapsedTime();
}

void AISPruner::Prune(MTRand& mt){
    timer.startTimer();
    initPopulation(mt);

    totalIterations = 0;
    while(!isConverged()){
        evaluateFitness();
        cloneAntibodies(mt);  	// Clone_And_Hypermutate
        addClonesToPopulation();
        sortAndTruncate();
        totalIterations++;
    }
    timer.stopTimer();
    pruningTime = timer.getElapsedTime();

  /*  cout << setprecision(6)
         << initTime     << " "
         << fitnessTime  << " "
         << affinityTime << " "
         << cloneTime    << " "
         << sortTime     << "\n";*/
}

bool AISPruner::isConverged() {
    bool retValue = Pruner::isConverged();
    double totalHammingDist = 0, localHammingDist = 0;
    double avgFitness = 0, stdDev = 0;

    if(useLogging && totalIterations > 1){
        for(unsigned int i=0; i<antibodies.size(); i++){
            avgFitness += antibodies[i].fitness;
        }
        avgFitness /= ((double)antibodies.size());

        for(unsigned int i=0; i<antibodies.size(); i++){
            stdDev += pow(antibodies[i].fitness - avgFitness,2);
        }
        stdDev /= (antibodies.size()-1.0);
        stdDev = sqrt(stdDev);
        stdDevFitness.push_back(stdDev);

        localHammingDist = 0;
        for(unsigned int i=0; i<antibodies.size(); i++){
            for(unsigned int j=0; j<antibodies.size(); j++){
                if(i != j){
                    localHammingDist = 0;
                    for(unsigned int k=0; k<antibodies[i].pos.size(); k++){
                        localHammingDist +=  abs(antibodies[i].pos[k] - antibodies[j].pos[k]);
                    }
                    totalHammingDist += localHammingDist;
                }
            }
        }
        hammingDiversity.push_back(totalHammingDist/antibodies.size());
    }

    return retValue;
}
