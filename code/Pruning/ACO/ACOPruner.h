/*
 * ACOPruner.h
 *
 *  Created on: Nov 11, 2010
 *      Author: rgreen
 */

#ifndef ACOPRUNER_H_
#define ACOPRUNER_H_

#include "Pruner.h"
#include "Ant.h"

class ACOPruner: public Pruner {
    public:

        ACOPruner(int np, int nt,  shared_ptr<Classifier>& o, std::vector<Generator> g, std::vector<Line> l, double p, double r, double t, bool ul=false);
        ACOPruner(int np, int nt,  shared_ptr<Classifier>& o, std::vector<Generator> g, std::vector<Line> l, double p, bool ul=false);
        void Init(int np, int nt,  shared_ptr<Classifier>& o, std::vector<Generator> g, std::vector<Line> l, double p, double r, double t, bool ul=false);

        void Reset(int np, int nt);

        void Prune(MTRand& mt);
        void initPheromones();
        void initPopulation(MTRand& mt);
        void updateSolutions(MTRand& mt);
        void evaluateFitness();
        void updatePheromones();

        void clearVectors();
        virtual ~ACOPruner();

    protected:
        bool isConverged();

    private:
        double rho, deltaT, bestFitness;
        int bestIndex;
        std::vector<double>    pMones;
        std::vector<Ant>    ants;

        double initPMoneTime, initPopTime;
        double updatePopTime, updatePMoneTime;
        double fitnessTime;
        
};

#endif /* ACOPRUNER_H_ */
