FROM alpine:latest

LABEL maintainer="Rob Green <greenr@bgsu.edu>" \
        description="This container is used to build MCS-Pruning, a peice of software that performs Non-Sequential Monte Carlo Simulation (MCS) in conjunction with Intelligent State Space Pruning for the probabilistic reliability evaluation of composite power systems, which is a complicated, computation intensive, and combinatorial task. As such evaluation may suffer from issues regarding high dimensionality that lead to an increased need for computational resources. Non-Sequential Monte Carlo Simulation (MCS) is often used to evaluate the reliability of power systems for various reasons. This software implements Non-Sequential MCS with multiple variants to improve performance."

RUN apk add -X https://dl-cdn.alpinelinux.org/alpine/v3.16/main -u alpine-keys
RUN echo "http://dl-cdn.alpinelinux.org/alpine/edge/community" >> /etc/apk/repositories;
RUN echo "http://dl-cdn.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories;

RUN apk update && apk add make g++ bash

# RUN mkdir /code
VOLUME /code
# RUN cd /code/Default && make all

# ENTRYPOINT ["bash"]

