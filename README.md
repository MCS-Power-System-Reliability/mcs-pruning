# MCS-PS #

The probabilistic reliability evaluation of composite power systems is a complicated, computation intensive, and combinatorial task. As such evaluation may suffer from issues regarding high dimensionality that lead to an increased need for computational resources. Non-Sequential Monte Carlo Simulation (MCS) is often used to evaluate the reliability of power systems for various reasons. This software implements Non-Sequential MCS with multiple variants to improve performance. 

## Related Publications ##
1. **R. C. G. II** and V. Agrawal, A case study in multi-core parallelism for the reliability evaluation of composite power systems, Journal of Supercomputing, vol. 73, no. 12,  pp. 5125–5149, December 2017.

1. **R. C. G. II**, L. Wang, and M. Alam, Intelligent State Space Pruning for Monte Carlo Simulation with Applications in Composite Power System Reliability. Engineering Applications of Artificial Intelligence, vol. 26, no. 7, pp. 1707-1724, 2013, 10.1016/j.engappai.2013.03.006
    
2. **R. C. G. II**, L. Wang, and M. Alam, Applications and Trends of High Performance Computing for Electric Power Systems: Focusing on Smart Grid, IEEE Transactions on Smart Grid, vol. 4, no. 2, pp. 922-931, June 2013.

3. **R. C. G. II**, L. Wang, and M. Alam, "Intelligent State Space Pruning with Local Search for Power System Reliability Evaluation," IEEE Power and Energy Society Innovative Smart Grid Technologies Europe, Berlin, Germany, October 2012.

4. **R. C. G. II**, L. Wang, M. Alam, and C. Singh, "Evaluating the Impact of Low Discrepancy Sequences on the Probabilistic Evaluation of Composite Power System Reliability," IEEE Power and Energy Society General Meeting 2012, San Diego, California, July 2012.

5. **R. C. G. II**, L. Wang, M. Alam, and C. Singh, "Latin Hypercube Sampling for the Probabilistic Evaluation of Composite Power System Reliability," Probabilistic Methods Applied to Power Systems, Istanbul, Turkey, June 2012.

6. **R. C. G. II**, L. Wang, M. Alam, and S. S. S. R. Depuru, "An Examination of Artificial Immune System Optimization in Intelligent State Space Pruning," North American Power Symposium 2011, Boston, Massachusetts, August 2011.

7. **R. C. G. II**, L. Wang, M. Alam, and S. S. S. R. Depuru, "Evaluating the Impact of Plug-in Hybrid Electric Vehicles on Composite Power System Reliability," North American Power Symposium 2011, Boston, Massachusetts, August 2011.

8. **R. C. G. II**, L. Wang, and M. Alam, “Composite power system reliability evaluation using support vector machines on a multicore platform,” IEEE International Joint Conference on Neural Networks, San Jose, California, August 2011.

9. **R. C. G. II**, L. Wang, M. Alam, and C. Singh, "Intelligent State Space Pruning Using Multi-Objective PSO for Reliability Analysis of Composite Power Systems: Observations, Analyses, and Impacts," IEEE Power and Energy Society General Meeting 2011 (PESGM 2011), Detroit, Michigan, July 2011. 

10. **R. C. G. II**, L. Wang, M. Alam, and C. Singh, "State space pruning for Reliability Evaluation using Binary Particle Swarm Optimization," Power Systems Exhibition and Conference, Phoenix, Arizona, March 2011.
     
11. **R. C. G. II**, L. Wang, M. Alam, and C. Singh, "Intelligent and Parallel State Space Pruning for Power System Reliability Analysis Using MPI on a Multicore Platform," Second Conference on Innovative Smart Grid Technologies (ISGT 2011), Anaheim, California, January 2011.

12. **R. C. G. II**, L. Wang, Z. Wang, M. Alam, and C. Singh, "Power System Reliability Assessment Using Intelligent State Space Pruning Techniques: A Comparative Study” 2010 Conference on Power System Technology, Hangzou China, October 2010.

13. **R. C. G. II**, L. Wang, and C. Singh, "State Space Pruning for Power System Reliability Evaluation using Genetic Algorithms," in Proceedings of the IEEE PES General Meeting, Minneapolis, MN, July 2010.
 
## Features ##

### Sampling ###
1. MCS Sampler
2. PSO Sampler
3. Latin Hypercube Sampler
4. Low Discrepancy Sequence Sampler
5. Descriptive Sampling Sampler
6. CFO Sampler
7. Antithetic Variate Sampling
8. Batch Sampling w/ OpenMP

### Classification ###
1. DC-OPF State Classification
2. Capacity State Classification
3. Neural Network State Classification
4. Support Vector Machine (SVM) State Classification

## Building the Software ##
To build the software, it is suggested that you use the included Dockerfile. If you would like to build the image yourself, you can use the following command first:

    docker build . -t <TAG_NAME>

To enter the docker container, simply use the command 

    docker run -v $PWD/code:/code -it rgreen13/mcs-pruning

That command will open the docker container into the project root directory and mount the local code directory to the code directory in the container. Once inside the container, input the following commands

    cd ./code
    cd ./Default
    make all
    ./MCS_Pruning


If you do not wish to use a container, simply begin in project root directory. Once in the directory input the following commands, same as if you were inside the container.

    cd ./code
    cd ./Default
    make all
    ./MCS_Pruning

### Dependencies ###
1. lpsolve
2. libsvm


## Contributing to the Code ##
1. Always use spaces for indentation

## Adding a Classifier ###
1. Create new .h and .cpp file in the Classification directory following the format "XXX_Classifier.h/cpp". I highly suggest using one of the other classification methods as a template.
2. Make sure that the new Classifier inherits the Classifier class.
3. Implement proper constructors, etc.
4. Override the run method

## Adding a Sampler ###
1. Create new .h and .cpp file in the Samplers directory following the format "XXX_Sampler.h/cpp". I highly suggest using one of the other sampler methods as a template
2. Make sure that the new Sampler inherits the Sampler class.
3. Implement proper constructors, etc.
3. Override run method
